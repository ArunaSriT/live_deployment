import numpy as np
from scipy import signal as spsig

def get_onsets(sig,find_limit=False):
    """
    Peak detection for ABP signals.

    Zong, W., et al.
    "An open-source algorithm to detect onset of arterial blood pressure pulses."
    Computers in Cardiology, 2003. IEEE, 2003.
    """
    const = 0.2

    sig = np.nan_to_num(sig,0)

    scale = sig.mean()
    offset = sig.max()-sig.mean()

    Araw = sig*scale - offset

    # Low-pass filter
    A = spsig.lfilter([1,0,0,0,0,-2,0,0,0,0,1],[1,-2,1],Araw)/24+30
    A = (A[3:]+offset)/scale # Takes care of 4 sample group delay


    # Slope-sum function
    dypos = np.diff(A)
    dypos = np.where(dypos<=0, 0, dypos)

    ssf = np.concatenate([
              np.array([0,0]),
              np.convolve(np.ones(16), dypos)]
          )

    # Decision rule
    avg0 = ssf[:1000].mean()
    threshold0 = 3*avg0
    lockout    = 0    # lockout>0 means we are in refractory
    timer      = 0
    z          = []#np.zeros(100000)
    # counter    = 0

    for t in range(50,len(ssf)-17+1): # -1 from MATLAB -> Python conversion
        lockout -= 1
        timer += 1     # Timer used for counting time after previous ABP pulse
        
        if (lockout<1) and (ssf[t]>avg0+const/2):  # Not in refractory and SSF has exceeded threshold here
            timer = 0
            maxSSF = ssf[t:t+16].max()  # Find local max of SSF
            minSSF = ssf[t-16:t].min()  # Find local min of SSF
            
            if maxSSF > (minSSF + const):
                onset = 0.01*maxSSF   # Onset is at the time in which local SSF just exceeds 0.01*maxSSF

                dssf     = ssf[t-16:t] - ssf[t-16-1:t-1]
                #beatTime = find(dssf<onset,1,'last')+t-17;
                beatTime = np.where(dssf<onset)[0]
                #counter  = counter+1;

                if len(beatTime)==0:
                    z.append(0)
                    #counter = counter-1;
                else:
                    beatTime = beatTime[-1]+t-17
                    z.append(beatTime)
                
                threshold0 = threshold0 + 0.1*(maxSSF - threshold0)  # adjust threshold
                avg0 = threshold0 / 3        # adjust avg

                lockout = 32   # lock so prevent sensing right after detection (refractory period)
            

        if timer > 312:  # Lower threshold if no pulse detection for a while
            threshold0 = threshold0 - 1
            avg0       = threshold0/3
        
    z = np.array(z)
    onsets = z[np.where(z!=0)[0]]-2
        
    # tag = np.random.randint(1e8)
    # np.savetxt(f'abp_fig_{tag}.txt',sig)
    # np.savetxt(f'abp_ons_{tag}.txt',onsets)
    return onsets