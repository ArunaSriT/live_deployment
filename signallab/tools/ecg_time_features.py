from scipy import stats
from scipy.signal import find_peaks, filtfilt, lfilter, butter
import math
import pywt
import numpy as np
from . import tools
from pdb import set_trace as st
from scipy import stats


def tci(signal_proc, fs):
    """
    THRESHOLD CROSSING INTERVAL parameter, based on:

    1) "Ventricular tachycardia and fibrillation detection by a sequential
    hypothesis testing algorithm"
    N.V Thakor, Y.S. Zhu, and K.Y. Pan,
    IEEE Trans Biomed Eng, 37(9): 837-843, 1990.

    2) "Reliability of old and new ventricular fibrillation detection
    algorithms for automated external defibrillators"
    A. Amann, R. Tratning, and K. Unterkofler,
    Biomed Eng Online, 4(60), 2005.

    --- INPUTS: ---
    signal_proc: ecg signal (preprocessed)
    fs: sampling frequency
    win: window length, in seconds

    --- RETURNS: ---
    tci (float): tci parameters
    """


    L = int(len(signal_proc)/fs)
    WN = L-3+1

    binary = tools.singal_binarization(signal_proc, fs)

    tci = np.zeros((WN))

    for i in range(WN):
        temp = binary[i*fs:(i+3)*fs]

        diff = np.concatenate(([0], np.diff(temp)))

        s1 = np.where(diff[:fs] == -1)[0]
        if s1.size > 0:
            t1 = (fs-s1[-1])/fs
        else:
            t1 = 1

        s2 = np.where(abs(diff[fs : fs*2]) == 1)[0]

        # Below has been added for when no crossings are detected.
        # The TCI is set to 1000, since it must be at least 1000ms
        # between threshold crossings.
        if len(s2)==0:
            tci[i] = 1000
            continue

        if diff[fs+s2[0]] == -1 and diff[fs+s2[-1]] == -1:
            t2 = 0
            t3 = (fs-s2[-1]) / fs
            n = (len(s2) + 1) / 2
        elif diff[fs+s2[0]] == 1 and diff[fs+s2[-1]] == -1:
            t2 = s2[0] / fs
            t3 = (fs-s2[-1]) / fs
            n = (len(s2)) / 2
        elif diff[fs+s2[0]] == -1 and diff[fs+s2[-1]] == 1:
            t2 = 0
            t3 = 0
            n = (len(s2) + 2) / 2
        elif diff[fs+s2[0]] == 1 and diff[fs+s2[-1]] == 1:
            t2 = s2[0] / fs
            t3 = 0
            n = (len(s2) + 1) / 2
        else:
            print('error on TCI calculation')

        s4 = np.where(diff[2*fs : 3*fs] == 1)[0]
        if s4.size > 0:
            t4 = s4[0]/fs
        else:
            t4 = 1

        tci[i] = 1000 / ((n-1) + (t2/(t1+t2)) + (t3/(t3+t4)))

    return np.mean(tci), np.std(tci)

def tcsc(signal_proc, fs):
    """
    THRESHOLD CROSSING SAMPLE COUNT parameter, based on:

    "A simple time domain algorithm for the detection of ventricular
    fibrillation in electrocardiogram"
    Arafat M, Chowdhury A and Hasan M,
    Signal, Image and Video Processing, 5: 1-10, 2011.

    --- INPUTS: ---
    signal_proc: ecg signal (preprocessed)
    fs: sampling frequency
    verbose: debugging variable (1: plot; 0: default, not ploting)

    --- RETURNS: ---
    tcsc (float): tcsc parameters
    """

    L = int(len(signal_proc)/fs)
    WN = L-3+1

    # The previous version of w (commented out below) did not work when fs != 250
    # (Gave an off-by-one error sometimes due to rounding)
#    w = np.concatenate((0.5 * (1 - np.cos(4 * np.pi * np.arange(0, 1/4, 1/fs))),\
#                        np.ones(int(2.5*fs)-1),\
#                        0.5* ( 1 - np.cos( 4 * np.pi * np.arange(0, 1/4, 1/fs)))))

    tvals = np.linspace(0,3,3*fs)
    part1 = np.where(tvals<0.25)[0]
    part2 = np.where((tvals>=0.25)&(tvals<3-0.25))[0]
    part3 = np.where(tvals>=3-0.25)[0]

    w = tvals.copy()*0
    w[part1] = 0.5 * (1 - np.cos(4 * np.pi * tvals[part1]))
    w[part2] = 1
    w[part3] = 0.5 * (1 - np.cos(4 * np.pi * tvals[part3]))

    n = np.zeros(WN)

    for i in range(WN):

        binary = np.zeros(3*fs)

        sig_temp = signal_proc[i*fs : (i+3)*fs]
        sig_temp = sig_temp * w
        sig_temp = sig_temp / np.max(sig_temp)

        thr = 0.2 * np.max(np.abs(sig_temp))
        binary[np.abs(sig_temp) > thr] = 1

        n[i] = np.sum(binary) / (3*fs)

    return np.mean(n), np.std(n)

def std_exp(signal_proc, fs):
    """
    EXPONENTIAL parameter, based on:

    "Reliability of old and new ventricular fibrillation detection
    algorithms for automated external defibrillators"
    A. Amann, R. Tratning, and K. Unterkofler,
    Biomed Eng Online, 4(60), 2005.

    --- INPUTS: ---
    signal_proc: ecg signal (preprocessed)
    fs: sampling frequency
    verbose: debugging variable (1: plot; 0: default, not ploting)

    --- RETURNS: ---
    std_exp (int): standard exponential parameter
    """
    L = int(len(signal_proc)/fs)
    WN = L-3+1

    n = np.zeros(WN)

    for i in range(WN):

        sig_temp = signal_proc[i*fs : (i+3)*fs]
        sig_temp = sig_temp / np.max(sig_temp)

        m = np.amax(np.abs(sig_temp))
        tm = np.argmax(np.abs(sig_temp))/fs
        t = np.arange(0, 3, 1/fs)

        e = m * np.exp(-(np.abs(t - tm) / 3))

        cross = np.sum(np.diff(e > sig_temp)) / 2

        n[i] = cross * 60 / 3

    return np.mean(n)

def mod_exp(signal_proc, fs):
    """
    MODIFIED EXPONENTIAL parameter, based on:
    "Reliability of old and new ventricular fibrillation detection
    algorithms for automated external defibrillators"
    A. Amann, R. Tratning, and K. Unterkofler,
    Biomed Eng Online, 4(60), 2005.

    --- INPUTS: ---
    signal_proc: ecg signal (preprocessed)
    fs: sampling frequency
    verbose: debugging variable (1: plot; 0: default, not ploting)

    --- RETURNS: ---
    mod_exp(int): modified exponential parameter
    """
    L = int(len(signal_proc)/fs)
    WN = L-3+1

    n = np.zeros(WN)

    for i in range(WN):

        sig_temp = signal_proc[i*fs : (i+3)*fs]
        sig_temp = sig_temp / np.max(np.abs(sig_temp))

        en = sig_temp

        cross = 0
        tm = 0
        peak = [0]
        while True:

            peak = find_peaks(sig_temp[peak[0]:], distance=0.2*fs)[0] + peak[0]
            if peak.size > 0:
                cross += 1
                m = sig_temp[peak[0]]
                tm = peak[0]/fs
                t = np.arange(0, 3, 1/fs)

                e = m * np.exp(-(np.abs(t - tm) / 0.2))
            else:
                break

            if peak[0]+int(0.2*fs) < 3*fs:
                x = np.where(sig_temp[peak[0]+int(0.2*fs):] > e[peak[0]+int(0.2*fs):])[0]
                if x.size > 0:
                    x = x[0]
                    x += peak[0]+int(0.2*fs)
                    en[peak[0]:x] = e[peak[0]:x]
                else:
                    break

        n[i] = cross * 60 / 3

    return np.mean(n)

def mva(signal_proc, fs):
    """
    MEAN ABSOLUTE VALUE parameter, based on:

    "Sequential algorithm for life threatening cardiac pathologies
    detection based on mean signal strength and EMD functions"
    E. Anas, S. Lee, and M. Hasan,
    Biomed Eng Online, 9(1), 2010.

    --- INPUTS: ---
    signal_proc: ecg signal (preprocessed)
    fs: sampling frequency
    verbose: debugging variable (1: plot; 0: default, not ploting)

    --- RETURNS: ---
    mva (float): mean absolute value parameter
    """

    L = int(len(signal_proc)/fs)
    WN = L-2+1

    mav = np.zeros(WN)

    for i in range(WN):
        sig_temp = signal_proc[ i*fs : (i+2)*fs ].copy()
        sig_temp = sig_temp / np.max(np.abs(sig_temp))

        mav[i] = (1 / (2*fs)) * np.sum( np.abs(sig_temp))

    return np.mean(mav)

def count(signal_proc, fs):
    """
    Band-pass filtering and auxiliary counts, based on:

    "Real time detection of ventricular fibrillation and tachycardia"
    I. Jekova, V. Krasteva, Physiol. Meas. 25, 1167-1178, 2004

    --- INPUTS: ---
    signal_proc: ecg signal (preprocessed)
    fs: sampling frequency
    verbose: debugging variable (1: plot; 0: default, not ploting)

    --- RETURNS: ---
    count1 (float): coiunt 1 parameter
    count2 (float): count 2 paramter
    count3b (float): count 3 parameter
    """
    # filter coefs definition, according to paper
    L = int(len(signal_proc)/fs)
    WN = L-1+1

    a = [8, -14, 7]
    b = [1/2, 0, -1/2]

    fsi = lfilter(b, a, signal_proc)
    fsi = np.abs(fsi)

    c1, c2, c3, c3b = np.zeros(L), np.zeros(L), np.zeros(L), np.zeros(L)

    for i in range(WN):
        fsi_temp = fsi[i*fs : (i+1)*fs]

        c1[i] = np.sum(fsi_temp >= 0.5*np.max(fsi_temp))

        c2[i] = np.sum(fsi_temp >= np.mean(fsi))

        md = np.mean(np.abs(fsi - np.mean(fsi)))

        c3[i] = np.sum((fsi_temp >= np.mean(fsi)-md) & (fsi_temp <= np.mean(fsi)+md))

    c3b = c1 * c2 / c3

    return np.mean(c1)/L, np.mean(c2)/L, np.mean(c3b)/L

def x(signal_proc, fs):
    """
    Slope and Frequency domain Features, based on:

    "A Reliable Method for Rhythm Analysis During Cardiopulmonary
    Resuscitation", Ayala U. et al

    --- INPUTS: ---
    signal_proc: ecg signal (preprocessed)
    fs: sampling frequency
    verbose: debugging variable (1: plot; 0: default, not ploting)

    --- RETURNS: ---
    x1 (float): x1 parameter
    x2 (float): x2 parameter
    """
    N = int(0.1*fs)

    slope = np.square( np.diff( signal_proc))

    slope = np.convolve(slope, np.ones((N,))/N, mode='same')

    slope = slope / np.max(slope)

    x1 = np.percentile(slope, 10)

    peaks = find_peaks(slope, distance=fs/10, height=0.1)[0]
    x2 = len(peaks)

    return x1, x2

def bcp(signal_proc):
    """
    Slope and Frequency domain Features, based on:

    "A high-temporal resolution algorithm to discriminate shockable
    from nonshockable rhythms in adults and children"
    Arusta U. et al

    --- INPUTS: ---
    signal_proc: ecg signal (preprocessed)
    fs: sampling frequency
    verbose: debugging variable (1: plot; 0: default, not ploting)

    --- RETURNS: ---
    bcp (float):
    """
    THR = 0.0055

    slope = np.square( np.diff( signal_proc))
    slope = slope / np.max(slope)

    bcp = np.sum(slope < THR) / slope.size

    return bcp

def vfleak(signal_proc):
    """

    --- INPUTS: ---
    signal_proc: ecg signal (preprocessed)
    fs: sampling frequency
    verbose: debugging variable (1: plot; 0: default, not ploting)

    --- RETURNS: ---
    vfleak (float): vfleak parameter
    """

    num = np.sum( np.abs( signal_proc[1:] ))
    den = np.sum( np.abs( signal_proc[1:] - signal_proc[:-1] ))

    N = math.floor( (np.pi*(num)/(den)) + 1/2)

    num = np.sum( np.abs( signal_proc[N:] + signal_proc[:-N]) )
    den = np.sum( np.abs(signal_proc[N:]) + np.abs(signal_proc[:-N]) )

    vf = num / den

    return vf

def spec(signal_proc, fs):
    """
    SPECTRAL ALGORITHM, based on:

    "Algorithmic sequential decision making in the frequency domain for
    life threatening ventricular arrythmias and imitative artifacts: a
    diagnostic system",
    S. Barro, R. Ruiz, D. Cabello, and J. Mira,
    Journal of Biomed Eng., 11(4): 320-8, 1989

    "Reliability of old and new ventricular fibrillation detection
    algorithms for automated external defibrillators"
    A. Amann, R. Tratning, and K. Unterkofler,
    Biomed Eng Online, 4(60), 2005

    --- INPUTS: ---
    signal_proc: ecg signal (preprocessed)
    fs: sampling frequency
    verbose: debugging variable (1: plot; 0: default, not ploting)

    --- RETURNS: ---
    m (float):
    a1 (float):
    a2 (float):
    a3 (float):
    """
    L = len(signal_proc)

    h = np.hamming(L)

    NFFT = 2**13

    y = np.fft.fft(signal_proc * h, NFFT)
    y = y[:int(NFFT/2)]
    f = np.arange(0, fs/2, (fs/2)/(NFFT/2))

    amp = np.abs(np.real(y)) + np.abs(np.imag(y))

    omega_f, omega_p = np.max( amp[(f>0.5) & (f<9)]), np.argmax( amp[(f>0.5) & (f<9)])

    omega_p = f[omega_p + np.argmax((f>0.5))]

    thr = 0.05 * omega_f

    amp[amp < thr] = 0

    jmax = np.min([20*omega_p, 100])

    # M
    wj = f[f<jmax]
    aj = amp[f<jmax]
    m = (1/omega_p) * (np.sum(aj * wj) / np.sum(aj))

    # A1
    a_num = amp[(f>0.5) & (f<omega_p/2)]
    a_den = amp[(f>0.5) & (f<jmax)]
    a1 = (1/omega_p) * (np.sum(a_num) / np.sum(a_den))

    # A2
    a_num = amp[(f>0.7*omega_p) & (f<1.4*omega_p)]
    a2 = (1/omega_p) * (np.sum(a_num) / np.sum(a_den))

    # A3
    a_num = amp[(f>2*omega_p) & (f<8*omega_p)]
    a3 = (1/omega_p) * (np.sum(a_num) / np.sum(a_den))

    return m, a1, a2, a3

def x_freq(signal_proc, fs):
    """
    Slope and Frequency domain Features, based on:

    "A Reliable Method for Rhythm Analysis During Cardiopulmonary
    Resuscitation", Ayala U. et al.

    --- INPUTS: ---
    signal_proc: ecg signal (preprocessed)
    fs: sampling frequency
    verbose: debugging variable (1: plot; 0: default, not ploting)

    --- RETURNS: ---
    x3 (float):
    x4 (float):
    x5 (float):
    """

    NFFT = 2048

    L = len(signal_proc)
    h = np.hamming(L)
    y = np.fft.fftshift(np.fft.fft(signal_proc * h, NFFT))

    f = np.arange(-fs/2, fs/2, fs/NFFT)

    psd = np.square(np.abs(y))
    psd = psd[(f>=0) & (f<=30)]
    f = f[(f>=0) & (f<=30)]
    psd = psd / np.sum(psd)

    x3 = f[np.argmax(psd[(f>=1) & (f<=10)]) + np.argmax(f>1)]
    x4 = np.sum(psd[(f>=2.5) & (f<=7.5)])
    x5 = np.sum(psd[f >= 12])

    return x3, x4, x5

def bwt(signal_proc, fs):
    """
    Frequency domain Features, based on:

    "A high-temporal resolution algorithm to discriminate shockable
    from nonshockable rhythms in adults and children"
    Arusta U. et al

    --- INPUTS: ---
    signal_proc: ecg signal (preprocessed)
    fs: sampling frequency
    verbose: debugging variable (1: plot; 0: default, not ploting)

    --- RETURNS: ---
    bwt (float):
    """
    nyq = 0.5 * fs
    low = 6.5 / nyq
    high = 30 / nyq
    b, a = butter(3, [low, high], btype='band')

    signal_filt = filtfilt(b, a, signal_proc)

    signal_filt = signal_filt / np.max(signal_filt)

    xh = np.percentile(signal_filt, 75)
    xl = np.percentile(signal_filt, 25)

    bwt = xh - xl

    return bwt

def cm(signal_proc):
    """
    COMPLEXITY MEASURE parameters, based on:

    "Detecting ventricular tachycardia and fibrillation by complexity
    parameter"
    X.S. Zhang, Y.S. Zhu, N.V. Thakor and Z.Z. Wang
    IEEE Trans. Biomed. Eng. 46(5): 548-55, 1999

    "Reliability of old and new ventricular fibrillation detection
    algorithms for automated external defibrillators"
    A. Amann, R. Tratning, and K. Unterkofler,
    Biomed Eng Online, 4(60), 2005

    --- INPUTS: ---
    signal_proc: ecg signal (preprocessed)
    fs: sampling frequency
    verbose: debugging variable (1: plot; 0: default, not ploting)

    --- RETURNS: ---
    cm (float): complexity measure paraemter

    --- IMPORTNANT ---
    the kolmogorov funtion implemented is an aproximation of the kolmoforov
    complexity. It is very slow and possibly inacurate. True kolmorov complecity
    is dificult or impossible to calculate. The use of this function is thus not
    recomended. Info:
    """
    print('read important function documentation')

    N = len(signal_proc)
    s = np.zeros(N)

    xi = signal_proc - np.mean(signal_proc)
    xmax = np.max(xi)
    xmin = np.min(xi)

    pc = np.sum( xi[(xi>0) & (xi<0.1*xmax)])
    nc = np.sum( xi[(xi<0) & (xi>0.1*xmin)])

    if (pc+nc) < 0.4*N:
        th = 0
    elif pc < nc:
        th = 0.2 * xmax
    else:
        th = 0.2 * xmin

    s[xi >= th]= 1

    # Complexity measure
    _, cm = kolmogorov(s)

    return cm


def jekova(signal_proc):
    """
    COVARIANCE, FREQUENCY, AREA, and KURTOSIS parameters, based on:

    "Shock advisory tool: detection of life-threatening cardiac
    arrhyrhmias and shock success prediction by means of a common
    parameter set"
    I. Jekova, Biomed. Sig. Proc. Control, 2:25-33, 2007

    "Ventricular Fibrillation and Tachycardia Classification uning a
    Machine Learning Approach",
    Q. Li, C. Rajagopalan and G.D. Clifford,
    IEEE Trans. Biomed. Eng. (In Press)

    --- INPUTS: ---
    signal_proc: ecg signal (preprocessed)
    fs: sampling frequency
    verbose: debugging variable (1: plot; 0: default, not ploting)

    --- RETURNS: ---
    cv_bin (float):
    frq_bin (float):
    area_bin (float):
    kurt_binv (float):
    """

    N = len(signal_proc)
    s = np.zeros(N)

    xi = signal_proc - np.mean(signal_proc)
    xmax = np.max(xi)
    xmin = np.min(xi)

    pc = np.sum( xi[(xi>0) & (xi<0.1*xmax)])
    nc = np.sum( xi[(xi<0) & (xi>0.1*xmin)])

    if (pc+nc) < 0.4*N:
        th = 0
    elif pc < nc:
        th = 0.2 * xmax
    else:
        th = 0.2 * xmin

    s[xi >= th]= 1

    # Convariance calculation
    cv_bin = np.var(s)

    # Frequency calculation
    frq_bin = np.sum( np.diff(s) == 1)

    # Area calculation
    area_bin = np.sum(s)

    # Kurtosis calculation
    kurt_bin = stats.kurtosis(s)

    return cv_bin, frq_bin, area_bin, kurt_bin


def kolmogorov(s):
    """
    Function for estimating the Kolmogorov Complexity as per:
    "Easily Calculable Measure for the Complexity of Spatiotemporal Patterns"
    by F Kaspar and HG Schuster, Physical Review A, vol 36, num 2 pg 842
    """

    n = len(s)
    c = 1
    l = 1

    i = 0
    k = 1
    k_max = 1
    stop = 0

    loop = 0
    while stop == 0:
        print('\r loop:{}, i:{}, l:{}, k:{}, c:{}'.format(loop, i, l, k, c), end='\r')
        if s[i+k] != s[l+k]:
            if k > k_max:
                k_max = k
            i += 1
            if i == l:
                c += 1
                l += k_max
                if l+1 > n:
                    stop = 1
                else:
                    i = 0
                    k = 1
                    k_max = 1
            else:
                k = 1
        else:
            k += 1
            if l+k >= n:
                c += 1
                stop = 1
        loop += 1
    b = n / np.log2(n)

    """ a la Lempel and Ziv (IEEE trans inf theory it-22, 75 (1976),
    h(n)=c(n)/b(n) where c(n) is the kolmogorov complexity
    and h(n) is a normalised measure of complexity """

    complexity = c / b

    return c, complexity

def li(signal_proc, fs):
    """
    Li feature, based on:

    "An Algorithm Used for Ventricular Fibrillation
    Detection Without Interrupting Chest Compression"
    Li et al

    --- INPUTS: ---
    signal_proc: ecg signal (preprocessed)
    fs: sampling frequency
    verbose: debugging variable (1: plot; 0: default, not ploting)

    --- RETURNS: ---
    li (float): Li feature
    """

    # parameters
    #return 0
    nMax = 6
    t_win = 0.3
    level = 3
    wave = 'db5'

    # wavelet transform
    n_pad = 2**level - (len(signal_proc) % 2**level)
    ecg_pad = np.pad(signal_proc, (0, n_pad), 'constant')
    ecg_cwt = pywt.swt(ecg_pad, wave, level=level)[0][1]
    ecg_mm = np.abs(ecg_cwt)**2

    peaks, properties = find_peaks( ecg_mm[int(t_win*fs):-int(t_win*fs)], distance=int(0.2*fs), height=0)
    peaks_s = [x for _,x in sorted(zip(properties['peak_heights'], peaks), reverse=True)]

    if len(peaks_s) < nMax:
        nMax = len(peaks_s)
    locs = peaks_s[:nMax]

    # Note, the below is NOT always the same as int(t_win*fs*2), 
    # for example with t_win=0.3 and fs=128, as with the NSRDB database.   
    beatlen = math.floor(t_win*fs)-math.floor(-t_win*fs)

    # template
    beat_cwt = np.zeros((beatlen, len(locs)))
    for i in range(nMax):
        interval  = np.arange(math.floor(-t_win*fs), math.floor(t_win*fs)) + peaks_s[i]
        beat_cwt[:,i] = ecg_cwt[interval]

    beat_mean = np.mean(beat_cwt, axis=1)

    # calcualte correaltiona and residuals
    xx_tw = np.correlate(beat_mean, beat_mean, mode='same')
    xx_tw = xx_tw / np.max(xx_tw)

    xy_tw = np.zeros((beatlen,len(locs)))
    res = np.zeros(len(locs))
    for i in range(nMax):
        xy_tw[:,i] = np.correlate(beat_mean, beat_cwt[:,i], mode='same')
        xy_tw[:,i] = xy_tw[:,i] / np.max(xy_tw[:,i])
        res[i] = np.sum( np.abs( xx_tw - xy_tw[:,i]))

    # modified feature
    li = np.percentile(res, 50)

    return li





















