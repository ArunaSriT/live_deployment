import numpy as np
import pandas as pd
from collections import defaultdict
from scipy import stats

def get_time_features(sig, fs=125., whichfeats=[], step=5, window=10):
        """
        Lameski, Petre, et al.
        "Suppression of intensive care unit false alarms based on the arterial blood pressure signal."
        IEEE Access 5 (2017): 5829-5836.
        """

        functions_dict = {
            'autocor_all':autocor_all,
            'quartiles':quartiles,
            'abp_stats':abp_stats,
        }

        functions = [functions_dict[f] for f in whichfeats if f in functions_dict.keys()]

        row_times = np.arange(window, len(sig)/fs, step)

        all_time_features = defaultdict(list)

        for time in row_times:
            start_index = int((time-window)*fs)
            end_index = int((time)*fs)
            interval = sig[start_index:end_index]

            all_time_features['seconds'].append(time)

            for fun in functions:
                some_feats = fun(interval)
                for sf in some_feats.keys():
                    all_time_features[f'abpfeat-time_{sf}'].append(some_feats[sf])

        time_features = pd.DataFrame(all_time_features)

        return time_features

def autocor_all(sig):
    def get_lagged(sig, lag):
        circular = 0
        m = sig.mean()
        a = sig-m
        b = np.roll(sig, lag)-m
        if not circular:
            a = a[lag:]
            b = b[lag:]
        return a, b
    
    def autocor(sig, lag):#
        a, b = get_lagged(sig,lag)
        return np.dot(a,b) / np.dot(a,a)

    def kendall(sig, lag):
        a, b = get_lagged(sig,lag)
        n = len(a)
        out = 0
        for i in range(n):
            for j in range(n):
                out += np.sign(a[i]-a[j])*np.sign(b[i]-b[j])
        return 2*out/(n*(n-1))

    out = {}
    for lag in [5, 10, 50]:
        out[f'autocor_{lag}'] = autocor(sig, lag)
        # Kendall has been disabled because it takes very long (O(n**2)).
        #out[f'kendall_{lag}'] = kendall(sig, lag)
    return out

def quartiles(sig):
    ssig = sorted(sig)
    out = {}
    out['q1'] = ssig[int(len(ssig)*0.25)]
    out['q2'] = ssig[int(len(ssig)*0.50)]
    out['q3'] = ssig[int(len(ssig)*0.75)]
    return out

def abp_stats(sig):
    """
    Whilst this is replicated somewhat in tools.signal_stats, the output form is
    different, and I didn't want to replicate all of the measures.
    """
    out = {}
    out['mean']  = sig.mean()
    out['var']  = np.var(sig, ddof=1)
    out['rms']  = np.sqrt(out['var'])
    out['kurt'] = stats.kurtosis(sig, bias=False)
    out['skew'] = stats.skew(sig, bias=False)
    return out




