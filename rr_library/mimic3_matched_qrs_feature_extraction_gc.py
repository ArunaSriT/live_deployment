import numpy as np

import os

import pickle

import gc

import time

from datetime import datetime

import adaf

from hjorth_mobility_complexity import hjorth

from detrended_fluctuation_analysis import dfa

from sampen_v2 import get_sampen

from freq_features import process_freq_features

import math

import pandas as pd

import multiprocessing as mp

from functools import partial


TARGET_LEADS = ['II']

QRS_DETECTORS = ['gqrs']



PATH_QRS = '/home/santos/gcs/MIMIC/diogo/qrs/mimic3wdb/matched/'



WIND_LEN = 240



def get_ectopic_freq(x):

    return np.sum(x) / WIND_LEN



def get_sampen_roll(x, rr):

    res = get_sampen(x.to_numpy().copy(), mm=3, r=0.2)



    rr.at[x.index[-1], 'SE1'] = res[0]

    rr.at[x.index[-1], 'SE2'] = res[1]

    rr.at[x.index[-1], 'SE3'] = res[2]

    rr.at[x.index[-1], 'SE4'] = res[3] 

    return 0



def get_freq_features(x, rr):

    res = process_freq_features(x.to_numpy())

    

    rr.at[x.index[-1], 'VLF'] = res[0]

    rr.at[x.index[-1], 'LF'] = res[1]

    rr.at[x.index[-1], 'HF'] = res[2]

    rr.at[x.index[-1], 'LF_HF'] = res[3]

    return 0



def get_hjorth(x, rr):

    res = hjorth(x.to_numpy())

    rr.at[x.index[-1], 'mobility'] = res[0]

    rr.at[x.index[-1], 'complexity'] = res[1]

    return 0



def get_dfa(x):

    return dfa(x)



def mimic3_matched_qrs_part(record,

                            plot_signal=True,

                            verbose=False):

    if verbose:

        print('   ', record)

        start = time.time()

    

    try:

        path_load = f"{PATH_QRS}{record}_quick.pkl"

        if not os.path.exists(f"{'/home/santos/'}{record[:12]}"):

            os.mkdir(f"{'/home/santos/'}{record[:12]}")

        path_save = f"{'/home/santos/'}{record}_qrs_feat.pkl"

        

        if os.path.exists(path_save):

            print('    file exists', record)

        else:

            data = pickle.load(open(path_load, 'rb'))

            rr = pd.DataFrame(data['II']['gqrs_rr'], columns=['rr'])

            amp = pd.DataFrame(data['II']['gqrs_amp'], columns=['qrs_amp'])[1:] # calculate mean and sd

            beat_corr = pd.DataFrame(data['II']['gqrs_beat_corr'], columns=['beat_corr'])[1:] # calculate mean, sd and number of ectopics

            



            drop_pause = (rr['rr'] < 10000).to_list()

            rr = rr[drop_pause]

            amp = amp[drop_pause]

            beat_corr = beat_corr[drop_pause]



            rr.loc[:, 'rr_adaf'], rr.loc[:, 'X'], ectopic_freq = adaf.ectopic_cleaning_v2(rr['rr'].to_numpy())

            rr['rr_adaf'] = rr['rr_adaf'].abs()

            rr['rr_vs_x'] = rr['X'] != rr['rr_adaf']

            

            rr.loc[:, 'rr_adaf_diff'] = rr['rr_adaf'].diff()

            

            roll = rr.rolling(window=WIND_LEN,

                              min_periods=WIND_LEN)

        

            rr['ectopic_freq'] =  roll['rr_vs_x'].apply(get_ectopic_freq,

                                                  raw=True,

                                                  engine='numba')

            

            rr['mean'] = roll['rr_adaf'].mean()

            

            rr['std'] = roll['rr_adaf'].std()

            

            rr['rr_adaf_diff'] = rr['rr_adaf'].diff()

        

            rr['std_diff'] = roll['rr_adaf_diff'].std()

            

            rr['sd1'] = rr['std_diff'] / math.sqrt(2.0)

            rr['sd2'] = np.sqrt(2.0 * rr['std']**2 - 0.5 * rr['sd1']**2)

            rr['sd12'] = rr['sd1'] / rr['sd2']

            

            roll['rr_adaf'].apply(get_sampen_roll,

                                  args=(rr,),

                                  raw=False)

            

            roll['rr_adaf'].apply(get_freq_features,

                                  args=(rr,),

                                  raw=False)    

            

            roll['rr_adaf'].apply(get_hjorth,

                                  args=(rr,),

                                  raw=False)

                

            rr['alpha'] = roll['rr_adaf_diff'].apply(get_dfa,

                                                   raw=True)

                    

            

            

            roll = amp.rolling(window=WIND_LEN,

                              min_periods=WIND_LEN)

            

            amp['amp_mean'] = roll['qrs_amp'].mean()

            amp['amp_std'] = roll['qrs_amp'].std()

            amp['amp_median'] = roll['qrs_amp'].median()

            amp['amp_skew'] = roll['qrs_amp'].skew()

            amp['amp_kurt'] = roll['qrs_amp'].kurt()

            

            

            roll = beat_corr.rolling(window=WIND_LEN,

                      min_periods=WIND_LEN)

            

            beat_corr['beat_corr_mean'] = roll['beat_corr'].mean()

            beat_corr['beat_corr_std'] = roll['beat_corr'].std()

            beat_corr['beat_corr_median'] = roll['beat_corr'].median()

            beat_corr['beat_corr_skew'] = roll['beat_corr'].skew()

            beat_corr['beat_corr_kurt'] = roll['beat_corr'].kurt()

            beat_corr['beat_corr_ectopic_freq'] = roll['beat_corr'].apply(lambda x: np.sum(x<0.9) / WIND_LEN,

                                                                raw=True)

            

            pd.concat([rr, amp, beat_corr], axis=1).to_pickle(path_save)

            

            

            # Record extracted samples

            with open('/home/santos/mimic3_qrs_features_extracted_done.csv', 'a+') as f:

                f.write(record + '\n')



    except Exception as e:

        if verbose:

            print('    ERRROOOOOORRRRRRRRR', e)

        # Record failed samples

        with open('/home/santos/mimic3_qrs_features_extracted_error.csv', 'a+') as f:

            f.write(record + ',' + '"' + str(e) + '"' + '\n')

    

    if verbose:

        end = time.time()

        print('    duration:', int(end - start), record)

    

if os.path.exists('/home/santos/mimic3_qrs_features_extracted_done.csv'):

    with open('/home/santos/mimic3_qrs_features_extracted_done.csv', 'r') as f:

        analsyed_samples = f.read().splitlines()

else:

    analsyed_samples = []



with open('./RECORDS-waveforms.txt', 'r') as f:

    full_records = f.read().splitlines()



signal_records = [x for x in full_records if x not in analsyed_samples]

n_cores = 10



if __name__ == '__main__':

    """

    len_ = len(signal_records)

    for r, record in enumerate(signal_records):

        print(r, int(r/len_*100))

        mimic3_matched_qrs_part(record)

    """

    """

    try:

        googlecloudprofiler.start(

            service='hello-profiler',

            service_version='1.0.1',

            # verbose is the logging level. 0-error, 1-warning, 2-info,

            # 3-debug. It defaults to 0 (error) if not set.

            verbose=3,

            # project_id must be set if not running on GCP.

            # project_id='my-project-id',

        )

    except (ValueError, NotImplementedError) as exc:

        print(exc)  # Handle errors here

    """

    with mp.Pool(n_cores) as p:

        p.map(partial(mimic3_matched_qrs_part,

                      plot_signal=False,

                      verbose=True), signal_records)

    

    

