# -*- coding: utf-8 -*-
import numpy as np
from scipy import interpolate, signal

def process_freq_features(rri, nperseg=256, fs=7.):
    """
    Compute frequency domain parameters
    """
    #Create time array
    t = np.cumsum(rri) / 1000
    t -= t[0]

    #Evenly spaced time array
    t_interp = np.arange(0, t[-1], 1 / fs)
    
    f = interpolate.interp1d(x=t,
                             y=rri,
                             kind='linear')
    
    rri_interp = f(t_interp)
    
    if nperseg > len(rri_interp):
        nperseg = len(rri_interp)
        
    #PSD with Welch's Method
    f, Pxx_den = signal.welch(rri_interp,
                              fs=fs,
                              window="hanning",
                              nperseg=nperseg,
                              detrend="linear")
    
    f_res = f[1] - f[0]
    
    idx = f <= 0.04
    vlf = np.trapz(Pxx_den[idx], dx=f_res)
    
    idx = np.logical_and(f >= 0.04, f <= 0.15)
    lf = np.trapz(Pxx_den[idx], dx=f_res)
    
    idx = np.logical_and(f >= 0.15, f <= 0.4)
    hf = np.trapz(Pxx_den[idx], dx=f_res)
    
    return vlf, lf, hf, lf/hf