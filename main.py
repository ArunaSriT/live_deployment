"""
@Date creation: 05.08.21
@author: Aruna T, Diem Bui
This file will do:
- Create a fastapi app with restful post api to receive the timeseries data 
- Publish the raw data to subscriber for storing raw data in both database and bucket storage
- Send the data to predictive model and return the output and send the output to database
- In the backgroud, send patients to Bucket and retrieve it when application is restarted
"""

from fastapi import FastAPI, BackgroundTasks
from fastapi_utils.tasks import repeat_every
from google.cloud.storage import bucket
from pydantic import BaseModel
from typing import Optional,List
import asyncio
import json
import gc

from tensorflow.keras.models import Model
from generation.interval_groups import IntervalGroup 
import pandas as pd
#from pubsub.starter import start, pubsub_run
from pubsub import starter
from handler import Handler
from patient import *
from patient import Warmup, Patient, Signal, PatientResponse, PatientValid, PatientError
from fastapi.middleware.cors import CORSMiddleware
#from pubsub import bgtstore
from pubsub.bgtable import BgTable

from dill import dumps, loads
from tensorflow import keras


#import joblib
#import pickle

class Packet(BaseModel):
    patientID: str
    fileID: str
    admissionID: str
    lead: str
    fs: int
    data: List[float]
    time: str


def save_patients(obj):

    # save the handler.patient object as pickle
    patient_bucket = config.get("patientscache")
    #starter.send_file_to_bucket(patient_bucket, dumps(handler), "handler.pkl", prefix="handler", delete=True)
    #pats =handler.patients
    starter.send_file_to_bucket(patient_bucket, dumps(obj), "patients.pkl", prefix="patients", delete=True)
    print('***************save pickle to bucket done')

def load_patients():
    """
    Retrieve handler.pkl from bucket
    """
    return starter.get_file_from_bucket(config.get("patientscache"), "patients.pkl", prefix="patients")
    #return starter.get_file_from_bucket(config.get("patientscache"), "handler.pkl", prefix="handler")

def get_application():
    app = FastAPI(title="Transformative AI", version="1.0.0")
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    return app

# create fastapi application
app = get_application()

"""

@app.on_event("startup")
@repeat_every(seconds=1 * 60, wait_first=True )  # 5 minutes 
def savepatients():
    save_patients()

"""
@app.get("/")
def home():
    # print(f"Handler object : {handler}")
    return {"message":"Transformative REST API Interface: send patient data and get CA score by post request update_predict "}

@app.post("/update_predict/")
async def update_predict(packet: Packet, background_tasks: BackgroundTasks):
    score=''
    if packet:
        packet = packet.dict()
        #print(packet)
        process_packet(packet)
        score= await(get_cascore(packet))
        background_tasks.add_task(save_patients, obj= handler.patients)
    return score



def process_packet(packet):
    #print('In process packet',packet)
    message = starter.pubsub_run(packet)
    print(f"call pubsub and return message:{message}")

def process_output(patient, output):
    """
    - Params:
        @patient is patient data sent through http request
        @output is a result returned from models. It can be a Warmup object or a dataframe
    - Return 
        A json with patientID, fileID and admissionID and list of score and list of timestamp
    """
    try:
        patientID = patient['patientID']
        fileID = patient['fileID']
        admissionID = patient['admissionID']
        time = patient['time']
        dict_ret = {'patientID': patientID, 'fileID': fileID, 'admissionID' : admissionID, 'time': time, 'scores' :[], 'timestamps': [] }
        if (type(output) is pd.DataFrame):
            ts = output['datetime'].values
            cascores = output['risk'].values
            dict_ret.update({'timestamps' : ts.tolist()} )
            dict_ret.update({'scores': cascores.tolist()})

            # send the result to bucket while waiting for database available
            # TODO Remove those lines after database is available
            # starter.send_data_to_bucket("fr-transformative-bucket-rawdata", dict_ret)
        bgtable.update_scores( dict_ret)
        #bgtstore.update_scores( dict_ret)

    except  Exception as e:
        print(e)
    return json.dumps(dict_ret, indent=2)


async def get_cascore(packet):
    output = handler.update_and_predict(packet)
    #print(f"predicted result: {output}")
    return process_output(packet, output)

with open('live_config.json', 'r') as fopen:
    config = json.load(fopen)
handler = Handler(config)
print('********main.py ******* ', config['algorithms']) 
#c_patients = len(handler.patients)

#patientscache = load_patients()
patientscache = None
print(f"+++++ patientscache {type(patientscache)} +++++")
# retrive pickle patients from the bucket
# if patients exist, update the patient attribute with this pickle patient
if (patientscache is not None):
    #handler.patients = loads(patientscache)
    patients = loads(patientscache)
    #"""
    print(f"+++++++ cache patients {type(patients)} +++++++++")
    for pat in patients:
        print(f"+++++++ cache patient {type(pat)}++++++++")
        #print(vars(pat.signals[0]))  
    #"""
    handler.set_patients(patients)
    del(patientscache)
    gc.collect()

#print(f"*******Handler() is created {handler}*********")
print(f"Number of patients : {len(handler.patients)} when handler is started")

print('********main.py after loading cache ******* ', handler.config['algorithms']) 
bgtable = BgTable('live_config.json')
print(f"******Bigtable is up*******")

starter.start()
print(f"******PubSub is up*******")



if __name__=='__main__':
#    with open('live_config.json', 'r') as fopen:
#        config = json.load(fopen)
#    handler = Handler(config=config)
#    print("Handler() is created")
    app.run()
    print("App is created")
