import datetime
import streamlit as st
import pandas as pd
from google.cloud import bigtable

from google.cloud.bigtable import column_family

from google.cloud.bigtable import row_filters

from google.cloud.bigtable.row_set import RowSet
from google.cloud.bigtable.row_filters import FamilyNameRegexFilter
import json
from datetime import datetime
import streamlit as st
import datetime
import os
from google.cloud.bigquery.client import Client # All Libararies dependencies

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'service-account-keys.json'
bq_client = Client() #Credentials to connect to bigtable

with open('live_config.json', 'r') as fopen:
   config = json.load(fopen)

project_id = config.get('project_id')#Reading the config details
instance_id = config.get('bgt_instance_id')
table_id = config.get('table_id')

client = bigtable.Client(project=project_id, admin=True)
instance = client.instance(instance_id)
table = instance.table(table_id)




row_set = RowSet() #reading rows to pick up only patScores
partial_rows = table.read_rows(
filter_=row_filters.FamilyNameRegexFilter("patScores".encode("utf-8")))
partial_rows.consume_all()

# result will be used later to create a dataframe and show table values by using Python dataframe API
result = {}

col_name = None

for row_key, row in partial_rows.rows.items():

    key = row_key.decode('ISO-8859–1')


    cells = row.cells['patScores'] # get all cells in the same col family

    if col_name is None:
        col_name = [k.decode('utf-8') for k in cells.keys()]

    # store rowise
    one_row_result = []

    for col_key, col_val in cells.items():
        value = col_val[0].value
        one_row_result.append(value.decode('ISO-8859–1'))
    result[key] = one_row_result






df = pd.DataFrame.from_dict(result, orient='index') #saving dict into dataframe for  operations
df.reset_index(inplace = True) #remove index
df.columns = ['patientid', 'timestamps']
jk=df #copy of dataframe
jk["timestamps"] = jk["timestamps"].apply(lambda x : dict(eval(x)) ) #separating the inner dictionary
output =pd.concat([jk.drop(['timestamps'], axis=1), jk['timestamps'].apply(pd.Series)], axis=1)
output =pd.DataFrame(output) #creating dataframe
output=output.set_index(['patientid']).apply(pd.Series.explode).reset_index()
output[['patientid','packettime']] = output.patientid.str.split("##",expand=True) #separating the patientid and packettime
output['Latestpackettime']=pd.to_datetime(output['packettime'], unit='s')
gh= output[output.groupby('patientid').Latestpackettime.transform('max') == output['Latestpackettime']] #take latest packettime details
#gh['packettimeconvert']=pd.to_datetime(gh['packettime'], unit='s')
b =gh['patientid'].unique() #take patientids who have a risk score to be displayed in dropdown


val =st.selectbox("Select the Patientid", b) #dropdown
si=gh.loc[(gh['patientid'] ==val) , ['patientid','timestamps','Latestpackettime','scores']] #pickup only these columns
si['scores'] =si['scores']*100 #convert into percentage
#st.write("Total PacketTime records available for this patient")
#output.loc[(output['patientid'] ==val), ['Latestpackettime']]
#st.write(val[0])
#val = st.text_input('Enter patient id')
output['timestamps']= pd.to_datetime(pd.Series(output['timestamps']))
#start_date = st.date_input('Select Date')
#start_date = start_date.strftime("%m/%d/%Y")
#st.write(start_date)

#val1 = st.text_input("Enter your timestamp: ")
#val1=  start_date+" "+val1
#s=output.loc[(output['timestamps'] == val1) & (output['patientid'] ==val), ['timestamps','scores']]
#s1= s['scores']*100
st.subheader('Latest Risk score for patient: ')
st.write(si) #printing scores
#st.write(s['timestamps'])
#st.write(s1)
#st.success("Data is available in Table")
