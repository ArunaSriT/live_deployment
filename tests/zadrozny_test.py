
import unittest

class Test_default(unittest.TestCase):
    # Just here to ensure the testing package is working.
    def test_default_pass(self):
        assert 2 + 2 == 4
