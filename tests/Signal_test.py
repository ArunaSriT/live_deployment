
# Add Zadrozny

import unittest
from patient import *
import numpy as np
import utils

global config
config = {
        "shapes_group_path": "models/fextract_16.pkl",
        "cnn_model_path": "models/cnn_best.h5",
        "finalmodel_type": "keras",
        "finalmodel_path": "models/m1_0_nolb.h5",

        "sample_data_path": "sample_data",
        "features_list_path": "feats_nolb.txt",

        "algorithms": [
            "ecg",
            "rr"
            ],

        "memory_time": 120,
        "seconds_in_sample": 10,
        "step": 10,
        "lockstep": True,

        "ecg_window": 10,

        "rr_beat_window": 240,

        "shapes_window": 10,

        "cnn_window": 10,
        "stack_size": 128,
        "cnn_batch_size": 64,
        "cnn_batch_jump": 100000,

        "bucket_name": "us-transformative-bucket-rawdata",
        "topic_name": "us-transformative-pubsub-topic-rawdata",
        "subscriber_1": "us-transformative-pubsub-subscription-storage",
        "subscriber_2": "us-transformative-pubsub-subscription-processing",
        "project_id": "transformative-deployment",
        "patientscache": "us-transformative-bucket-patientscache",
        "instance_id": "webapp-instance",
        "table_id" : "patientdata-table"
        }

global defaultargs
defaultargs = {
    'patientID': 'SDDB_999',
    'fileID': '01',
    'admissionID': '01',
    'lead': 'II',
    'fs': 250,
    'config': config,
    'peaks_given': False,
    'featuremodels': None,
    }

def datagen(fs, seconds):
    datalen = int(fs*seconds)
    data = np.random.random(datalen) + np.sin(np.arange(datalen)/4)
    return data



# ------------------------------------------------------------------------------
# -------------------------- Base tests ----------------------------------------
# ------------------------------------------------------------------------------



class Test_default(unittest.TestCase):
    # Just here to ensure the testing package is working.
    def test_default_pass(self):
        assert 2 + 2 == 4
    

    
class Base_tests():
    def test_right_diff(self):
        assert np.abs(np.diff(self.signal.times)-(1/self.args['fs'])).sum()<1e-4,\
            'Times have incorrect differential'

    def test_data_time_len(self):
        assert len(self.signal.data)==len(self.signal.times),\
            'Data and times have different lengths'



# ------------------------------------------------------------------------------
# -------------------------- Single packet -------------------------------------
# ------------------------------------------------------------------------------



class Base_test_single_packet_short(Base_tests):
    # setUpClass is a special method for unittests. It is called once at the 
    # initialisation of the class (like an __init__) and must be
    # decorated with @classmethod. It takes the class itself (cls) as an argument.
    # cls is like self, but it applies to ALL instances of the class, as well
    # as the class itself.
    @classmethod
    def setUpClass(cls):
        cls.data = cls.get_fixeddata(cls.params)
        cls.signal = cls.get_signal(cls.params)

    # setUp is another special method. It is run before every test.
    # Exceptions here are considered errors, not failed tests.
    def setUp(self):
        pass
    
    @classmethod
    def get_signal(cls, params):
        data = cls.data
        signal = Signal(**cls.args)
        signal.update_sig(newdata=data, attime=0)
        return signal

    @classmethod
    def get_signal_from_single_data(cls, data, args=None):
        if args is None:
            cls.args = defaultargs.copy()
        signal = Signal(**cls.args)
        signal.update_sig(newdata=data, attime=0)
        return signal
    
    @classmethod
    def get_fixeddata(cls, params):
        fs = params['fs']
        seconds = params['seconds']
        data = datagen(fs=fs, seconds=seconds)
        return data
    
    
    def test_len_report(self):
        # Here, we account for zero-length case.
        if self.__class__ in [Test_one_empty_data]: self.skipTest('Zero length')
        assert len(self.signal.report_times)==1, 'Return value length > 1.'

    def test_ret_val(self):
        if self.__class__ in [Test_one_empty_data]: self.skipTest('Zero length')
        assert self.signal.report_times[0]==0, 'Return value != 10.'
    
    def test_out_data_is_in_len(self):
        assert len(self.signal.data)==len(self.data),\
            'Data length is not equal to input length.'

    def test_out_data_is_in(self):
        # Note, this is only true since we are starting on 0, and using lockstep.
        if self.__class__ in [Test_one_nan_data]: self.skipTest('NaN data')
        assert (self.signal.data==self.data).all(), 'Data is not equal to input.'

# ------------------------------------------------------------------------------

class Test_one_fs_right_seconds_right(Base_test_single_packet_short, unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.params = {'fs': 250, 'seconds': 10}
        cls.args = defaultargs.copy()
        cls.args['fs'] = cls.params['fs']
        super().setUpClass()
            
class Test_one_short_seconds(Base_test_single_packet_short, unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.params = {'fs': 250, 'seconds': 7}
        cls.args = defaultargs.copy()
        cls.args['fs'] = cls.params['fs']
        super().setUpClass()
            
class Test_one_short_fs(Base_test_single_packet_short, unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.params = {'fs': 125, 'seconds': 7}
        cls.args = defaultargs.copy()
        cls.args['fs'] = cls.params['fs']
        super().setUpClass()
            
class Test_one_float_seconds(Base_test_single_packet_short, unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.params = {'fs': 250, 'seconds': 7.5}
        cls.args = defaultargs.copy()
        cls.args['fs'] = cls.params['fs']
        super().setUpClass()

class Test_one_nan_data(Base_test_single_packet_short, unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.data = np.array([np.nan]*(250*10))
        cls.signal = cls.get_signal_from_single_data(cls.data)
        cls.args = defaultargs.copy()

class Test_one_empty_data(Base_test_single_packet_short, unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.data = np.array([])
        cls.signal = cls.get_signal_from_single_data(cls.data)
        cls.args = defaultargs.copy()



# ------------------------------------------------------------------------------
# --------------------------- Two packets --------------------------------------
# ------------------------------------------------------------------------------



class Base_two_short_packets(Base_tests):
    # This is for two packets, where the total length doesn't exceed the
    # cutoff time.
    @classmethod
    def setUpClass(cls):
        cls.args = defaultargs.copy()
        if 'fs' in cls.params.keys():
            cls.args['fs'] = cls.params['fs']
        cls.signal = cls.get_signal()

    def setUp(self):
        pass

    @classmethod
    def get_signal(cls):
        data1 = datagen(fs=cls.params['fs'], seconds=cls.params['seconds_1'])
        data2 = datagen(fs=cls.params['fs'], seconds=cls.params['seconds_2'])

        signal = Signal(**cls.args)
        signal.update_sig(newdata=data1, attime=cls.params['attime_1'])
        signal.update_sig(newdata=data2, attime=cls.params['attime_2'])

        return signal

    def test_two_data_right_len(self):
        if self.__class__==Test_two_packets_second_empty: self.skipTest('Not applicable')
        last_time = self.params['attime_2'] + self.params['seconds_2']
        total_time = last_time - self.params['attime_1']
        n_points = total_time*self.args['fs']
        assert len(self.signal.data) == n_points

    def test_two_last_time(self):
        if self.__class__==Test_two_packets_second_empty: self.skipTest('Not applicable')
        last_time = self.params['attime_2'] + self.params['seconds_2']
        assert self.signal.times[-1]==last_time-1./self.args['fs']
    
    def test_two_first_time(self):
        assert self.signal.times[0]==self.params['attime_1']

# ------------------------------------------------------------------------------

class Test_two_packets_2x10s_correct(Base_two_short_packets, unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.params = {
            'fs': 250,
            'seconds_1': 10,
            'seconds_2': 10,
            'attime_1': 0,
            'attime_2': 10,
            }
        super().setUpClass()
    
class Test_two_packets_2x10s_overlapped(Base_two_short_packets, unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.params = {
            'fs': 250,
            'seconds_1': 10,
            'seconds_2': 10,
            'attime_1': 0,
            'attime_2': 5,
            }
        super().setUpClass()

class Test_two_packets_2x10s_overlapped_float(Base_two_short_packets, unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.params = {
            'fs': 250,
            'seconds_1': 10,
            'seconds_2': 10,
            'attime_1': 0,
            'attime_2': 7.5,
            }
        super().setUpClass()

class Test_two_packets_first_nan(Base_two_short_packets, unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.params = {
            'fs': 250,
            'seconds_1': 10,
            'seconds_2': 10,
            'attime_1': 0,
            'attime_2': 10,
            }
        cls.args = defaultargs.copy()
        if 'fs' in cls.params.keys():
            cls.args['fs'] = cls.params['fs']

        data1 = np.array([np.nan]*(
            cls.params['fs']*cls.params['seconds_1']
            ))
        data2 = datagen(fs=cls.params['fs'], seconds=cls.params['seconds_2'])

        cls.signal = Signal(**cls.args)
        cls.signal.update_sig(newdata=data1, attime=cls.params['attime_1'])
        cls.signal.update_sig(newdata=data2, attime=cls.params['attime_2'])

class Test_two_packets_second_nan(Base_two_short_packets, unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.params = {
            'fs': 250,
            'seconds_1': 10,
            'seconds_2': 10,
            'attime_1': 0,
            'attime_2': 10,
            }
        cls.args = defaultargs.copy()
        if 'fs' in cls.params.keys():
            cls.args['fs'] = cls.params['fs']

        data1 = datagen(fs=cls.params['fs'], seconds=cls.params['seconds_2'])
        data2 = np.array([np.nan]*(
            cls.params['fs']*cls.params['seconds_1']
            ))

        cls.signal = Signal(**cls.args)
        cls.signal.update_sig(newdata=data1, attime=cls.params['attime_1'])
        cls.signal.update_sig(newdata=data2, attime=cls.params['attime_2'])
        
class Test_two_packets_first_empty(Base_two_short_packets, unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.params = {
            'fs': 250,
            'seconds_1': 10,
            'seconds_2': 10,
            'attime_1': 0,
            'attime_2': 0,
            }
        cls.args = defaultargs.copy()
        if 'fs' in cls.params.keys():
            cls.args['fs'] = cls.params['fs']

        data1 = np.array([])
        data2 = datagen(fs=cls.params['fs'], seconds=cls.params['seconds_2'])

        cls.signal = Signal(**cls.args)
        cls.signal.update_sig(newdata=data1, attime=cls.params['attime_1'])
        cls.signal.update_sig(newdata=data2, attime=cls.params['attime_2'])

class Test_two_packets_second_empty(Base_two_short_packets, unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.params = {
            'fs': 250,
            'seconds_1': 10,
            'seconds_2': 10,
            'attime_1': 0,
            'attime_2': 10,
            }
        cls.args = defaultargs.copy()
        if 'fs' in cls.params.keys():
            cls.args['fs'] = cls.params['fs']

        data1 = datagen(fs=cls.params['fs'], seconds=cls.params['seconds_2'])
        data2 = np.array([])

        cls.signal = Signal(**cls.args)
        cls.signal.update_sig(newdata=data1, attime=cls.params['attime_1'])
        cls.signal.update_sig(newdata=data2, attime=cls.params['attime_2'])



# ------------------------------------------------------------------------------
# --------------------- Many packets involving cutoff --------------------------
# ------------------------------------------------------------------------------



class Base_after_cutoff(Base_tests):
    @classmethod
    def setUpClass(cls):
        cls.args = defaultargs.copy()
        if hasattr(cls,'fs') and 'fs' in cls.params.keys():
            cls.args['fs'] = cls.params['fs']

        cls.signal = Signal(**cls.args)
        for p in cls.packets:
            cls.signal.update_sig(newdata=p['data'], attime=p['attime'])
    
    def setUp(self):
        pass

    @classmethod
    def produce_packets(cls, datas, attimes):
        assert len(datas)==len(attimes)
        packets = []
        for i in range(len(datas)):
            packets.append({
                'data':datas[i],
                'attime':attimes[i],
                })
        return packets

    @classmethod
    def get_early_late_time(cls, which):
        end_times = [] # These are the latest times represented by the packets
        for p in cls.packets:
            p_end = p['attime'] + len(p['data']) * cls.args['fs']
            end_times.append(p_end)
        if which=='early': return min([p['attime'] for p in cls.packets])
        if which=='late':  return max(end_times)
        else: raise NotImplementedError

    def test_cutoff_times(self):
        early = self.get_early_late_time(which='early')
        late  = self.get_early_late_time(which='late')
        maxtime = self.signal.times.max()
        mintime = self.signal.times.min()
        assert np.round(maxtime - mintime, 8) == self.args['config']['memory_time']

# ------------------------------------------------------------------------------

class Test_cutoff_regular(Base_after_cutoff, unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        datas = [datagen(250,10) for i in range(15)]
        attimes = [0.+(10.*i) for i in range(15)]
        cls.packets = cls.produce_packets(datas, attimes)
        super().setUpClass()

class Test_cutoff_wrong_order(Base_after_cutoff, unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        datas = [datagen(250,10) for i in range(15)]
        attimes = [0.+(10.*i) for i in range(15)]

        datas[-2],   datas[-1]   = datas[-1],   datas[-2]
        attimes[-2], attimes[-1] = attimes[-1], attimes[-2]

        cls.packets = cls.produce_packets(datas, attimes)
        super().setUpClass()

class Test_cutoff_gap(Base_after_cutoff, unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        datas = [datagen(250,10) for i in range(15)]
        attimes = [0.+(10.*i) for i in range(15)]
        attimes = np.array(attimes)

        attimes[3:] += 45

        cls.packets = cls.produce_packets(datas, attimes)
        super().setUpClass()

class Test_cutoff_small_float_gap(Base_after_cutoff, unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        datas = [datagen(250,10) for i in range(15)]
        attimes = [0.+(10.*i) for i in range(15)]
        attimes = np.array(attimes)

        attimes[3:] += 0.4124212221110001

        cls.packets = cls.produce_packets(datas, attimes)
        super().setUpClass()



# ------------------------------------------------------------------------------
# ---------------------------- Peaks -------------------------------------------
# ------------------------------------------------------------------------------



# No base class for peaks, since there are a few cases with different set-ups.

def get_peaked_data(fs, seconds, seconds_between_peaks):
    datalen = int(fs*seconds)
    data = np.zeros(datalen)
    data += np.random.randn(datalen)/200

    peak_gap = int(seconds_between_peaks * fs)
    peak_locations = peak_gap * np.arange(1, datalen)
    peak_locations = peak_locations[np.where(peak_locations<datalen)[0]]

    data[peak_locations] += 1.5
    data[(peak_locations+1)[np.where(peak_locations<datalen-1)[0]]] += 0.5
    data[(peak_locations-1)] += 0.5

    return data

# ------------------------------------------------------------------------------

class Base_peaks():
    def test_peaks_diff(self):
        assert (
            np.abs(
                np.diff(self.peaks) - \
                self.seconds_between_peaks * self.fs
                ) < 5
            ).all()

    def test_peaks_0(self):
        assert self.peaks[0] == self.seconds_between_peaks * self.fs

# ------------------------------------------------------------------------------

class Test_peaks_calculate(Base_peaks, unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.fs = 250
        cls.seconds_between_peaks = 0.9
        data = get_peaked_data(cls.fs, 30, cls.seconds_between_peaks)
        args = defaultargs.copy()
        cls.signal = Signal(**args)
        cls.signal.update_sig(newdata=data, attime=0)
        cls.peaks, _ = cls.signal.calculate_peaks(cls.signal.data, cls.fs)

# class Test_peaks_given(Base_peaks, unittest.TestCase):
#     @classmethod
#     def setUpClass(cls):
#         #TODO Do here
#         pass


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------



if __name__=='__main__':
    c = Test_two_packets_first_empty()
    c.setUpClass()
    c.test_right_diff()
    print('done')