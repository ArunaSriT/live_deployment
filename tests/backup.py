from patient import *
import pytest
import json
import numpy as np


# The fixture decorator means that the output of this function can be used within
# other tests. If scope is set to "module", then this is only run once, 
# and the output is used in all test places where it is passed as an argument.
# If scope is set to "function", then this function is re-run in each 
# relevant test.

# On passing a parameter to a fixture: https://stackoverflow.com/questions/18011902/pass-a-parameter-to-a-fixture-function

# Note - parameterised tests can also be done with 
# @pytest.mark.parameterize('var1,var2', [(v11, v21), (v12, v22)])

global config
config = {
        "shapes_group_path": "models/fextract_16.pkl",
        "cnn_model_path": "models/cnn_best.h5",
        "finalmodel_type": "keras",
        "finalmodel_path": "models/m1_0_nolb.h5",

        "sample_data_path": "sample_data",
        "features_list_path": "feats_nolb.txt",

        "algorithms": [
            "ecg",
            "rr"
            ],

        "memory_time": 600,
        "seconds_in_sample": 10,
        "step": 10,
        "lockstep": True,

        "ecg_window": 10,

        "rr_beat_window": 240,

        "shapes_window": 10,

        "cnn_window": 10,
        "stack_size": 128,
        "cnn_batch_size": 64,
        "cnn_batch_jump": 100000,

        "bucket_name": "us-transformative-bucket-rawdata",
        "topic_name": "us-transformative-pubsub-topic-rawdata",
        "subscriber_1": "us-transformative-pubsub-subscription-storage",
        "subscriber_2": "us-transformative-pubsub-subscription-processing",
        "project_id": "transformative-deployment",
        "patientscache": "us-transformative-bucket-patientscache",
        "instance_id": "webapp-instance",
        "table_id" : "patientdata-table"
        }

global defaultargs
defaultargs = {
    'patientID': 'SDDB_999',
    'fileID': '01',
    'admissionID': '01',
    'lead': 'II',
    'fs': 250,
    'config': config,
    'peaks_given': False,
    'featuremodels': None,
    }

class Base_signal:
    #@pytest.fixture(scope='class')
    def datagen(self, params):
        fs = params['fs']
        seconds = params['seconds']
        args = defaultargs.copy()
        args['fs'] = fs
        datalen = int(fs*seconds)
        data = np.random.random(datalen) + np.sin(np.arange(datalen)/4)
        return data

# ------------------------------------------------------------------------------

class Base_test_single_packet_short(Base_signal):
    @pytest.fixture(scope='class')
    def signal(self, datagen, params):
        args = defaultargs.copy()
        args['fs'] = params['fs']
        signal = Signal(**args)
        signal.update_sig(newdata=datagen, attime=0)
        return signal    
    
    def test_len(self, signal):
        assert len(signal.report_times)==1, 'Return value length > 1.'

    def test_ret_val(self, signal):
        assert signal.report_times[0]==0, 'Return value != 10.'
    
    def test_out_data_is_in_len(self, signal, datagen):
        assert len(signal.data)==len(datagen), 'Data length is not equal to input length .'

    def test_out_data_is_in(self, signal, datagen):
        # Note, this is only true since we are starting on 0, and using lockstep.
        assert (signal.data==datagen).all(), 'Data is not equal to input.'

    def test_right_diff(self, signal):
        assert np.abs(np.diff(signal.times)-(1/defaultargs['fs'])).sum()<1e-6, 'Times have incorrect differential'

    def test_data_time_len(self, signal, thing):
        assert thing==3
        assert len(signal.data==signal.times), 'Data and times have different lengths'


class Test_fs_right_seconds_right(Base_test_single_packet_short):
    @pytest.fixture(scope='class')
    def params(self):
        return {
            'fs': 250,
            'seconds': 10
            }
class Test_short_seconds(Base_test_single_packet_short):
    @pytest.fixture(scope='class')
    def params(self):
        return {
            'fs': 250,
            'seconds': 7
            }
class Test_short_fs(Base_test_single_packet_short):
    @pytest.fixture(scope='class')
    def params(self):
        return {
            'fs': 125,
            'seconds': 10
            }
class Test_float_seconds(Base_test_single_packet_short):
    @pytest.fixture(scope='class')
    def params(self):
        return {
            'fs': 250,
            'seconds': 7.5
            }

# ------------------------------------------------------------------------------

class Base_two_packets(Base_signal):
    @pytest.fixture(scope='class')
    def signal(self, datagen, params):

        signal = Signal(**params['args'])
        
        data1 = datagen
        data2 = datagen
        data1 = data1[:int(params['fs']*params['seconds_length_1'])]
        data2 = data2[:int(params['fs']*params['seconds_length_2'])]

        signal.update_sig(newdata=data1, attime=params['attime_1'])
        signal.update_sig(newdata=data2, attime=params['attime_2'])

        return signal

class Test_two_packets(Base_two_packets):
    # Yes, this includes repetition of code. This is due to the strange
    # nature of PyTest.
    @pytest.fixture(scope='class')
    def params(self, pre_params):
        out = {}
        
        # Edit these 
        fs = 250
        out['seconds_length_1'] = 0
        out['seconds_length_2'] = 10
        out['attime_1'] = 0
        out['attime_2'] = 10

        args = defaultargs.copy()
        args['fs'] = fs
        out['fs']  = fs
        out['args'] = args

        # This should remain fixed. datagen only sees params, where it expects
        # a single value of seconds. We can't therefore, set two different 
        # seconds values, and call datagen twice. Instead, we use seconds_1
        # and seconds_2 to chop the data.
        out['seconds'] = 100 
        
        return out
class Test_two_packets(Base_two_packets):
    @pytest.fixture(scope='class')
    def params(self, pre_params):
        out = {}

        fs = 250
        out['seconds_length_1'] = 0
        out['seconds_length_2'] = 10
        out['attime_1'] = 0
        out['attime_2'] = 10

        args = defaultargs.copy()
        args['fs'] = fs
        out['fs']  = fs
        out['args'] = args
        out['seconds'] = 100 
        
        return out