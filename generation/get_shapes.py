import numpy as np
import pandas as pd
from scipy import signal as sps

import utils
from utils import ErrMessage

feat_name_meanvec = [f"shapefeat_meanvec_{x}" for x in range(16)]
feat_name_closestvec = [f"shapefeat_meanvec_{x}" for x in range(16)]
feature_names_all = [*feat_name_meanvec,
                      *feat_name_closestvec,
                      'shapefeat_mindistance',
                      'shapefeat_stdmin',
                      'shapefeat_stdmax']

n_points_interval = 64



def process(config, data, fs, peaks, eval_times, model=None):
    window = config['shapes_window']
    intervals, peak_times = utils.get_intervals(data, fs, peaks, resample_to=64)#reample NEEDED?XX

    #XX
    # newintervals = np.zeros([intervals.shape[0], n_points_interval])
    # for i, beat in enumerate(intervals):
    #     newintervals[i] = sps.resample(beat, n_points_interval)

    if model is None:
        raise Exception('No model given.')
    if len(intervals)==0:
        return ErrMessage('nointervals')

    transformed_vecs = model.transform(intervals)

    # Get the names
    meta_feature_names = [
            'shapefeat_meanvec',
            'shapefeat_closestvec',
            'shapefeat_mindistance',
            'shapefeat_stdmin',
            'shapefeat_stdmax'
            ]

    # Initialise the columns to 0
    data_dict = {'seconds':eval_times,}
    for mfn in meta_feature_names:
        if 'vec' in mfn:
            for x in range(model.n_clusters):
                data_dict[f'{mfn}_{x}'] = 0
        else:
            data_dict[f'{mfn}'] = 0
    df = pd.DataFrame(data_dict)

    # Get the meta-vars
    for i_et,et in enumerate(eval_times):
        eval_indices = np.where((peak_times<et)&(peak_times>et-window))[0]
        if len(eval_indices)==0:
            continue
        eval_vecs = transformed_vecs[eval_indices, :]

        for mfn in meta_feature_names:
            columns = [c for c in df.columns if mfn in c]
            feats = get_meta(eval_vecs, mfn)
            df.loc[i_et, columns] = feats
    return df

def get_meta(array, meta):
    if '_meanvec' in meta:
        return array.mean(axis=0)
    elif '_closestvec' in meta:
        return array[np.argmin(array.min(axis=1)),:]
    elif meta=='shapefeat_mindistance':
        return array.min()
    elif meta=='shapefeat_stdmin':
        if len(array.shape)==1 or array.shape[0]==1:
            return 0
        else:
            return (array.std(axis=0)).min()
    elif meta=='shapefeat_stdmax':
        if len(array.shape)==1 or array.shape[0]==1:
            return 0
        else:
            return (array.std(axis=0)).max()
