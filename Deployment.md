
---

# Transformative AI Deployment App Deployment on GCP App Engine and VM
  



## Deployment Instructions


- Connect to VM (fastapi-deployment) by ssh 
    gcloud compute ssh fastapi-deployment
-  Git clone repo ; cd live_deploymemt
-  Download models, accessories folder
-  Conda activate livedep
- To deploy the app in local VM: 
    - uvicorn main:app   --reload --port 5500    
    - Access api with https://127.0.0.1:5500/update_predict
    - To run the simulation tests:
       -- python pubsub/simulation.py 
- To deploy the app in app engine 
    - Gcloud app deploy      
    - Access api with https://coderhythm.transformative.ai/update_predict/
	- To See the log : 
		   gcloud app logs tail -s default
	- To test with simulator 
       -- copy sample_data folder and run 
	   -- python pubs/simulation.py

        