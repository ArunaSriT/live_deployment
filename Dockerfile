from continuumio/anaconda3

WORKDIR /app

COPY  .  /app

#RUN ls -la /app
# Make RUN commands use `bash --login`:
#SHELL ["/bin/bash", "--login", "-c"]
RUN apt-get update && apt-get clean

RUN conda env create -f /app/environment.yml

#RUN conda update -n base -c defaults conda
#RUN conda activate dockerlivedep
#ENV PATH /opt/conda/envs/dockerlivedep/bin:$PATH

# Make RUN commands use the new environment:
SHELL ["conda", "run", "-n", "dockerlivedep", "/bin/bash", "-c"]

# Initialize conda in bash config fiiles:
#RUN conda init bash

#RUN echo "conda activate dockerlivedep" >> ~/.bashrc
#SHELL ["/bin/bash", "--login", "-c"]
#RUN conda init bash
#RUN set -euo pipefail
#RUN conda activate dockerlivedep

# install python dependencies
#RUN pip install --upgrade pip
RUN pip install -r /app/requirements.txt

RUN python -c "import uvicorn"
RUN python -c "import fastapi"
#EXPOSE 8000

RUN ls -la /app

RUN conda env list

RUN echo $PATH


#RUN pip install fastapi uvicorn

#CMD gunicorn -b :8000  main:app
#expose 8000
#CMD uvicorn main:app 
ENTRYPOINT ["conda", "run", "--no-capture-output", "-n", "dockerlivedep", "uvicorn", "main:app","--reload","--host", "0.0.0.0", "--port", "8000"]