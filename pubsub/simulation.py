import os
import wfdb
#import hjson as json
import json
import datetime
import requests
import time

#url = 'https://transformative-deployment.uc.r.appspot.com/update_predict/'
url = 'https://coderhythm.transformative.ai/update_predict/'
#url = 'https://tr-backend-dot-transformative-deployment.uc.r.appspot.com/update_predict/'
#url = 'http://127.0.0.1:5500/update_predict/'
path = os.getcwd() + os.path.sep + "../testui/sample_data"
#path = os.getcwd() + os.path.sep + "sample_data"

# From sv_dev_1
# url = 'http://transformative-deployment.uc.r.appspot.com/update_predict/'
# url = 'http://127.0.0.1:8000/update_predict/'
# path = os.getcwd() + os.path.sep + "sample_data"

files = os.listdir(path)
# only get the dat file
dat_files = [f[:f.index('.dat')] for f in files if '.dat' in f]
print(path)
print(dat_files)

seconds_in_sample = 180
timeformat = r"%m/%d/%Y %H:%M:%S"
machine_on_time = datetime.datetime.strptime("07/25/2020 09:00:00", timeformat)

#start=0
start = 0
end = 9
for f in dat_files[start:end]:
    print(f"file {f}")
    sig, head = wfdb.rdsamp(os.path.join(path, f))
    fileID = os.path.split(f)[-1]
    
    fs = head['fs']
    print(f"fs: {fs}")
    print(f"head {head}")
    signal = sig[:,0].tolist()
    patientID = 'SDDB_' + fileID

    n = 30
    for i in range(n):
        print('packet :', i)
        sample_length = int(fs*seconds_in_sample)
        ecg = signal[i*sample_length:(i+1)*sample_length]
        start_time = machine_on_time + datetime.timedelta(seconds=10*i)
        start_time = datetime.datetime.strftime(start_time, timeformat)
        print(f"fs {fs}")
        packet = {
            'patientID': patientID,
            'fileID': fileID,
            'admissionID': '',
            'lead': 'II',
            'fs': fs,
            'data': ecg,
            'time': start_time
        }
        print(f"fs {fs}")

        start_time = time.time()
        print(url)
        response = requests.post(url, json = packet)
        print("Response time : ", time.time() - start_time)
        print("Status code: ", response.status_code)
        print("Printing Entire Post Request")
        if(response.status_code==200):
            print(response.json())
        else:
            print(response.text)

        # Wait for 5 seconds
        #print("Waiting for 20 seconds")
        #time.sleep(5)
        
        #print(packet)
