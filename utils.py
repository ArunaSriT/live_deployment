import numpy as np
import pandas as pd
import datetime
import pytz
import os
from os.path import join as opj
import multiprocessing as mpc
import itertools
from scipy import signal as sps

KNOWN_DBS = ['MIMIC2','MIMIC3','NEMC','SDDB','CUDB','NSRDB','MITDB','AJOU']

def make_unique(inlist):
    for i in inlist:
        assert type(i)==str
    out = inlist.copy()
    dupes = []
    for x in out:
        if out.count(x)>1:
            dupes.append(x)
    dupes = list(set(dupes))
    for d in dupes:
        counter = 1
        for i in range(len(out)):
            if out[i]==d:
                out[i] = f'{d}_{counter}'
                counter += 1
    return out

def process_with_mpc(patients,function,ncores=4,params=[]):
    """
        General utility function for distributing a bunch of patients 
        (or anything else) over a function in parallel.

        Parameters
        ----------
        patients : list
            Will usually be a list of patients, but can be any list

        function : function
            The function to distribute.

        ncores: int or str, optional
            How many cores to use. Default 4. If 'all', then the maximum 
            number are used.

        params : list, optional
            List of other parameters to pass to the function, in the correct
            order. Default [].
    """
    if ncores=='all':
        ncores = mpc.cpu_count()
    boundaries = np.linspace(0,len(patients),ncores+1)
    boundaries[-1] = len(patients)
    mpc_sets = []

    for i in range(len(boundaries)-1):
        sec_start = int(boundaries[i])
        sec_end   = int(boundaries[i+1])
        mpc_sets.append(patients[sec_start:sec_end])

    pool = mpc.Pool(ncores)
    for set_ in mpc_sets:
        pool.apply_async(
            function,
            [set_,*params],
            callback=lambda x: x)
    
    pool.close()
    pool.join()
    print("Pool's closed")

def evenly_spaced(*iterables):
    """
        From https://stackoverflow.com/questions/19293481/how-to-elegantly-interleave-two-lists-of-uneven-length-in-python
        >> evenly_spaced(range(10), list('abc'))
        [0, 1, 'a', 2, 3, 4, 'b', 5, 6, 7, 'c', 8, 9]
    """
    return [item[1] for item in
        sorted(itertools.chain.from_iterable(
        zip(itertools.count(start=1.0 / (len(seq) + 1), 
                    step=1.0 / (len(seq) + 1)), seq)
        for seq in iterables))]

def roundup(x, nearest, pr=None):
    x=x.copy()
    nearest = float(nearest)
    out = np.ceil(x / nearest) * nearest
    if pr is not None:
        out = np.round(out, pr)
    return out

def rounddown(x, nearest, pr=None):
    if type(x)==list or type(x)==np.ndarray:
        x=x.copy()
    nearest = float(nearest)
    out = np.floor(x / nearest) * nearest    
    if pr is not None:
        out = np.round(out, pr)
    return out

def roundfloat(x, nearest, pr=None):
    if type(x)==list or type(x)==np.ndarray:
        x=x.copy()
    nearest = float(nearest)
    out = np.round(x / nearest) * nearest    
    if pr is not None:
        out = np.round(out, pr)
    return out


def expand_dict(dict_in, remove=[]):
    """
        Takes a dictionary with lists as elements, and expands this into a list of dictionaries.

        Parameters
        ----------
        dict_in : dict
            The input dictionary. Each element should be a list.

        remove : list, optional
            Keys to remove from the dictionary. For example, keys for which dict_in is not a list. By default []

        Returns
        -------
        list    
            A list of dictionaries.

        Examples
        --------
        >>> d = {'a':[1,2], 'b':[3,4]}
        >>> expand_dict(d)
        [{'a':1, 'b':3}, {'a':1, 'b':4}, {'a':2, 'b':3}, {'a':2, 'b':4}]
    """
    out = []
    for k in remove:
        dict_in.pop(k)
    keys = list(dict_in.keys())
    product = itertools.product(*(dict_in[p] for p in keys))
    for item in product:
        out.append({
            keys[i]:item[i]
            for i in range(len(keys))
            if keys[i] not in remove
            })
    return out


def trim_nans(filt, trim='fb'):
    """
        Trim the leading and/or trailing zeros from a 1-D array or sequence.
        Based on np.trim_zeros

        Parameters
        ----------
        filt : 1-D array or sequence
            Input array.

        trim : str, optional
            A string with 'f' representing trim from front and 'b' to trim from
            back. Default is 'fb', trim zeros from both front and back of the
            array.

        Returns
        -------
        trimmed : 1-D array or sequence
            The result of trimming the input. The input data type is preserved.
    """
    first = 0
    trim = trim.upper()
    if 'F' in trim:
        for i in filt:
            if np.isnan(i) == False:
                break
            else:
                first = first + 1
    last = len(filt)
    if 'B' in trim:
        for i in filt[::-1]:
            if np.isnan(i) == False:
                break
            else:
                last = last - 1
    return filt[first:last]

def interp(sig_in, trim=True):
    """
        Interpolates the nans in a signal.

        Parameters
        ----------
        sig_in : np.array
            Input signal

        Returns
        -------
        np.array
            Same signal but with nans linearly interpolated.
    """
    sig = sig_in.copy()
    if trim:
        sig = trim_nans(sig)
    nans = np.isnan(sig)
    x = lambda z: np.nonzero(z)[0]
    if (nans.astype(int)-1).sum()==0:
        # if whole signal is nan, set to zero
        sig[nans]=0
    else:
        sig[nans] = np.interp(x(nans),x(~nans),sig[~nans])
    del(nans, x)
    return sig

def coshuffle(*items, seed=42):
    """
        Shuffles an arbitrary number of items in place while
        preserving mutual order.

        Parameters
        ----------
        item : iterable, any amount
            Any number of iterables. Mutual order is 
            preserved if lengths are the same.

        seed : int, optional
            [description], by default 42
    """
    assert all(items[i]==items[i+1] for i in range(len(items)-1))
    for thing in items:
        np.random.seed(seed)
        np.random.shuffle(thing)

def write_error(path, item, index, outof, error, stdout=False):
    rightnow = datetime.datetime.now(pytz.timezone('Europe/London')).strftime("%d/%m/%y %H:%M:%S")
    line1 = f'Item: {item} ({index}/{outof})\n'
    line2 = f'Error: {error}\n'
    line3 = f'At: {rightnow}\n'
    errstring = line1 + line2 + line3 + '\n'
    with open(path,'a+') as ferr:
        ferr.write(errstring)
    if stdout:
        print(errstring)

def get_intervals(data, fs, peaks, points_before=None, points_after=None, resample_to=None):
    """
        We get the separate beats surrounding the peaks.

        Parameters
        ----------
        data : np.ndarray
            The input signal. Assumed to be temporally continuous.

        fs : int
            Sampling rate.

        peaks : np.ndarray
            The indices (not times) of the peaks.

        points_before : int, optional
            Number of points before the peak to capture, by default None.
            If None, this is calculated from the fs as 0.1 seconds.

        points_after : int, optional
            Number of points after the peak to capture, by default None
            If None, this is calculated from the fs as 0.2 seconds.

        resample_to: int or None
            How many point to have in the interval. Set to None to 
            deactivate resampling.

        Returns
        -------
        all_intervals : np.ndarray
            An array of size [num intervals, num points around each peak], 
            containing the separated intervals.

        interval_times : np.ndarray
            The times at which the peaks occur. This is peaks / fs.s
    """
    max_nan_frac = 0.35

    if points_before is None:
        points_before = int(0.1*fs)
    if points_after is None:
        points_after = int(0.2*fs)

    all_intervals    = []
    interval_times   = []

    for ip in range(0,len(peaks)-1):
        peak = peaks[ip]
        this_interv = data[peak-points_before:peak+points_after]
        if len(this_interv)==0:
            continue
        
        nan_frac = np.isnan(this_interv).astype(int).sum()/len(this_interv)
        if nan_frac>max_nan_frac:
            continue
        else:
            this_interv = interp(this_interv)
            if resample_to is not None:
                # resample_to was 64 in original
                this_interv = sps.resample(this_interv, resample_to)
            
            # tolist added below on 22-06-21. Unclear why it wasn't needed before.
            all_intervals.append(this_interv.tolist())
            interval_times.append(peak/fs)
    
    all_intervals  = np.array(all_intervals) 
    interval_times = np.array(interval_times)

    return all_intervals, interval_times

def get_case(recordID, allow_fail=False):
    db, filename = recordID.split('_', 1)
    assert db in KNOWN_DBS
    if db=='MIMIC3': return 0
    if filename + '.dat' in os.listdir(f'/hdd/{db}_source/data/case/'):
        return 1
    elif filename + '.dat' in os.listdir(f'/hdd/{db}_source/data/control/'):
        return 0
    else:
        if allow_fail:
            return None
        else:
            raise FileNotFoundError(f'Cannot find file {filename} in case or control of /hdd/{db}_source/data.')

def minmax(min_, x, max_):
    x = max(min_, x)
    x = min(max_, x)
    return x

def find_earliest_nonnan(df, tags, maxrows):
    checkcols = [c for c in df.columns if any(t in c for t in tags)]
    for irow in range(min(maxrows,len(df))):
        row = df.iloc[irow]
        if not np.isnan(row[checkcols].values).all():
            break
    return irow

class ErrMessage:
    def __init__(self, message):
        self.message = message

def delt_string(tend, tbeg):
    mins = (tend-tbeg)//60
    secs = (tend-tbeg)%60
    sout = f'{int(mins)}m {int(secs)}s'
    return sout

def chunker(seq, size):
    """
        Produces an interable over an iterable, where each item is the next
        subset of given size from the input.

        Parameters
        ----------
        seq : array-like
            Input iterable

        size : int
            Size of each item in output.

        Returns
        -------
        array-like
            Iterable over the input.

        Examples
        --------
        >>> x = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        >>> chunker(x, 3)
        ([1, 2, 3], [4, 5, 6], [7, 8, 9], [10])
    """
    return [seq[pos:pos + size] for pos in list(range(0, len(seq), size))]

def rightnow():
    return datetime.datetime.now(pytz.timezone('Europe/London')).strftime("%d/%m/%y %H:%M:%S")

def timechopped(t0, t1):
    delta = datetime.timedelta(seconds=(t1-t0))
    return str(delta - datetime.timedelta(microseconds=delta.microseconds))

def update_dataframe(old, new, on):
    """
        This is a form of merging two dataframes. For each column, if the value at a given row is in one
        dataframe but not the other, then that value will be taken. If it is in both, then the new version will 
        be taken.

        Parameters
        ----------
        old : pd.DataFrame
            The first dataframe.

        new : pd.DataFrame
            The one with priority values.

        on : list
            The columns on which to merge.

        Returns
        -------
        pd.DataFrame
            The merged dataframe.
    """
    assert (old.columns == new.columns).all()
    columns = old.columns

    df_merged = pd.merge(
        old,
        new,
        on=on,
        how='outer',
        indicator=True,
        suffixes=['_fromold', '_fromnew']
        ).reset_index(drop=True)

    datanew = {x:[] for x in columns}
    
    for irow in range(len(df_merged)):
        for c in columns:

            if c in on:
                datanew[c].append(df_merged.loc[irow, c])

            elif df_merged.loc[irow,'_merge']=='left_only':
                datanew[c].append(df_merged.loc[irow, c+'_fromold'])

            elif df_merged.loc[irow,'_merge'] in ['right_only', 'both']:
                datanew[c].append(df_merged.loc[irow, c+'_fromnew'])

    datanew = pd.DataFrame(datanew)
    return datanew