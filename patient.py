from utils import rounddown, roundfloat
import os
from os.path import join as opj
import numpy as np
import pandas as pd
import json
import datetime
import pytz
import time
from icecream import ic
import pickle

# Move signallab relevant parts into this repo?
from signallab import SignalLab, tools
from signallab.tools.tools import filtering
from signallab.tools.ecg_qrs_drs import correct_rpeaks
from wfdb.processing.qrs import gqrs_detect as gqrs
from generation import get_ecg, get_lookback, get_shapes, get_cnn, get_rr
from utils import ErrMessage

# General error classes for handling these.
class PatientResponse:
    def __init__(self, message=None):
        self.message = message

class PatientValid(PatientResponse): pass
class AutocalculateOff(PatientValid): pass
class Warmup(PatientValid): pass
class Empty(PatientValid): pass

class PatientError(PatientResponse): pass
class UnknownError(PatientError): pass

class Patient():
    def __init__(
            self,
            patientID,
            config,
            featuremodels=None, # Might need to rearrange
            timeformat=None,
            zbins=None # Can give as an arg, or be loaded from config.
            ):
        self.patientID = patientID
        self.signals = []
        self.config = config
        self.algorithms = self.config['algorithms']
        if timeformat is None:
            timeformat = r"%m/%d/%Y %H:%M:%S"
        self.timeformat = timeformat

        # This should be the same models object across all patients and 
        # signals. This is created in Handler class, and passed down to here
        # and then to each Signal object.
        # Note that featuremodels are the cnn and shapes models.
        # The actual prediction on these models is done in the Handler class.
        self.featuremodels = featuremodels

        # ------- Zadrozny variables ------- #
        if zbins is None:
            self.load_background_zbins(self.config['zbins_json_path'])
        else:
            self.zbins = zbins

        # These variables store the calculated risk values and associated time
        # values (unixtime). The z_ is a reminder than these are used for 
        # Zadrozny calculations.
        self.z_risk_values = np.array([])
        self.z_risk_times = np.array([])

        # self.z_vt_within_tau is an array of the same length as the others.
        # self.z_vt_within_tau[i]=1 if self.z_risk_times[i] is within tau (the 
        # prediction window) of an arrhythmia, and 0 otherwise.
        self.z_vt_within_tau = np.array([])

        # self.z_occupancies[i,j] is 1 if self.z_risk_values[i] fits into bin j,
        # and is 0 otherwise. This sparse array is used since it is easy to sum
        # over at evaluation time.
        self.z_occupancies = np.array([])
        # ---------------------------------- #

        # This is a dataframe that is a concatenation of all reports 
        # produced for the patient. 
        self.reports = pd.DataFrame()

    def convert_time(self, attime):
        # Gets the time since 01/01/1970 00:00:00
        nseconds = datetime.datetime.strptime(attime, self.timeformat).timestamp()
        return int(nseconds)

    def load_background_zbins(self, path):
        """
            Loads the Zadrozny bins info from disk.


            Parameters
            ----------
            path : str
                Path to the location of the pre-calculated bins (pickle file).
                The zbins object should be a list of dictionaries, and is loaded
                directly from this file.
                

            Creates
            ------
            self.zbins : list
                self.zbins is a list of dictionaries which will contain saved
                info on the number of actual positives and negatives in the bin.
                An example element of this list:
                
                {'left_value': 0,
                'right_value': 0.095,
                'n1': 0,
                'n0': 909,
                'ratio': 0.0}

            self.nzbins : int
                The number of zbins.


            Notes
            -----
            The config should contain the following Zadrozny-related elements:

            • zbins_json_path : str
                The path to the pickle file containing the background bins info.
                This is passed to this function as an argument.

            • zadrozny_patient_specific_multiplier : int
                The number of times to add in a new patient's data.
                The relative weight of a patient to the background.

            • prediction_window : int
                The number of seconds before an arrhythmia for which a positive
                prediction counts as a true positive.
        """
        with open(path, 'rb') as fopen:
            self.zbins = pickle.load(fopen)
        self.nzbins = len(self.zbins)
    
    def update_pat(self, jsondata, autocalculate=True):
        assert jsondata['patientID']==self.patientID
        for sig in self.signals:
            if (
                sig.fileID == jsondata['fileID'] and 
                sig.lead == jsondata['lead']
                ):
                signal = sig
                assert sig.fs==jsondata['fs'] # fs should not change
                break
        else:
            peaks_given = True if jsondata.get('peaks', None) is not None else False
            signal = Signal(
                patientID=jsondata['patientID'],
                fileID=jsondata['fileID'],
                admissionID=jsondata['admissionID'],
                lead=jsondata['lead'],
                fs=jsondata['fs'],
                config=self.config,
                peaks_given=peaks_given,
                featuremodels=self.featuremodels,
                timeformat=self.timeformat
                )
            self.signals.append(signal) # Should keep persistent

        time_seconds = self.convert_time(jsondata['time'])
        
        signal.update_sig(
            newdata=jsondata['data'],
            attime=time_seconds,
            newpeaks=jsondata.get('peaks', None)
            )

        if not autocalculate:
            return AutocalculateOff()
            
        if (len(signal.data)/signal.fs)/60>self.config.get('wait_time_mins', 3): # <-------- New line
            df_all_report = signal.get_features()
            if self.config.get('keep_reports_in_patient_object', True):
                self.reports = pd.concat(self.reports, df_all_report)
                self.reports = self.reports.reset_index(drop=True)
            return df_all_report
        else:
            return Warmup()

    def update_zadrozny(self, risks, utimes):
        """
            This adds to the internal variables that track risk scores and 
            Zadrozny bin numbers for a patient. This is a Patient-level
            function, rather than a Signal-level function, since it is assumed
            that such information should be preserved across different admissions
            of a patient. This is particularly true, since modifications to a 
            patient's risk score can take a long time to build up, and we do not
            want this to be lost if a signal is restarted.

            Parameters
            ----------
            risks : np.ndarray
                A new set of predicted risk scores.

            utimes : np.ndarray
                The times of the above risk scores in unix time.
        """
        
        # We require a single risk score for each time and vice-versa.
        assert len(risks)==len(utimes)
        if type(risks)==list: risks = np.array(risks)
        if type(utimes)==list: utimes = np.array(utimes)
        
        # This is an array where element i, j is 1 if risk i is in bin j.
        new_occupancies = np.zeros([
            len(risks),
            self.nzbins
            ])

        # Populate the occupancies - get the bins for new risk scores.
        for ib, bin_ in enumerate(self.zbins):
            inbin_inds = np.where(
                (risks>bin_['left_value']) &
                (risks<=bin_['right_value'])
                )
            new_occupancies[inbin_inds, ib] = 1

        # Update the internal variables with the new variables.
        self.z_risk_values = np.concatenate([self.z_risk_values, risks])
        self.z_risk_times = np.concatenate([self.z_risk_times, utimes])
        self.z_occupancies = np.concatenate([self.z_occupancies, new_occupancies])\
            if len(self.z_occupancies)>0 else new_occupancies

    
    def modify_zadrozny(self, risks, utimes):
        """
            This modifies a set of risk scores by the Zadrozny procedure. Unlike
            in the training pipeline, update and modification are here split into
            two functions. This does mean there is a very slight (and resource-light)
            repetition of logic, since the bin must be found for a risk score twice.
            However, this allows for a much cleaner separation of these two logical 
            functions, allowing us to modify without changing the underlying variables.

            Parameters
            ----------
            risks : np.ndarray
                A new set of predicted risk scores.
                
            utimes : np.ndarray
                The times of the above risk scores in unix time.

            Returns
            -------
            np.ndarray
                The risk scores modified by the Zadrozny procudure.

            Raises
            ------
            Exception
                Unlikely to occur. This is thrown if no bin is found for a given
                risk score. If this is reached, then it is likely then the 
                original background bins were not created correctly.
        """
        
        tau = self.config.get('prediction_window', 3600)
        psm = self.config.get('zadrozny_patient_specific_multiplier', 1)

        # We require a single risk score for each time and vice-versa.
        assert len(risks)==len(utimes)
        if type(risks)==list: risks = np.array(risks)
        if type(utimes)==list: utimes = np.array(utimes)

        # This will be the output. Risk scores modified to account for Bayes.
        calibrated = risks.copy()

        for i, r in enumerate(risks):
            
            # Get the bin again for this risk value.
            for ib, bin_ in enumerate(self.zbins):
                if (r>bin_['left_value']) and (r<=bin_['right_value']):
                    break
            else:
                raise Exception('No bin found')

            # The time of the current risk being analysed.
            t = utimes[i]

            # last_mod_ind is the latest index in the self.z_* arrays for which
            # the results should be used to modify the current risk score.
            # This should be at least tau in the past, where tau is the 
            # prediction window.
            # np.searchsorted is a very fast method for finding the largest
            # index in "a" for which the value is smaller than a specified "v".
            # It assumes "a" increases monotonically.
            last_mod_ind = np.searchsorted(
                a = self.z_risk_times,
                v = t - tau
                )

            # n0_new is the number of non-arrhythmic predictions in the relevant 
            # bin in the appropriate time range. Since most rows will be
            # non-arrhythmic, we initially assume all rows contribute to n0.
            # We then do a np.where to check for self.z_vt_within_tau == 1 in that
            # same time range, and add these to n1 and subtract them from n0.
            # This is faster than checking where it is zero, since we want to 
            # preserve the speed gains from searchsort.
            n0_new = self.z_occupancies[:last_mod_ind, ib].sum()

            n1inds = np.where(self.z_vt_within_tau==1)[0]
            n1inds = n1inds[n1inds<last_mod_ind] #TODO < or <=?

            n1_new = self.z_occupancies[n1inds, ib].sum()
            n0_new -= n1_new

            n0_bg = bin_['n0']
            n1_bg = bin_['n1']

            n0_tot = n0_bg + psm * n0_new
            n1_tot = n1_bg + psm * n1_new

            r_prime = n1_tot / (n1_tot + n0_tot)
            calibrated[i] = r_prime

        return calibrated

class Signal():
    def __init__(
            self,
            patientID,
            fileID,
            admissionID,
            lead,
            fs,
            config,
            peaks_given,
            featuremodels=None,
            timeformat=None
            ):
        self.patientID = patientID
        self.fileID = fileID
        self.admissionID = admissionID
        self.lead = lead
        self.fs = fs
        self.diff = round(1/self.fs, 8)
        self.peaks_given = peaks_given # 1 iff RR peaks in json. Should be consistent.
        self.config = config
        self.step_index = int(config['step'] * self.fs)
        self.data  = np.array([])
        self.times = np.array([])
        self.report_times = np.array([])
        self.featuremodels = featuremodels
        self.pr = 8 # Precision, decimal places
        self.timeformat = timeformat

        # This is a dataframe with three columns for data, times, and ispeak. 
        # This allows us to use the filtering features of pandas.
        self.df_datatimes = pd.DataFrame({'data':[], 'time':[], 'ispeak':[]})

    def update_sig(self, newdata, attime, newpeaks=None):
        """
            Takes a new set of data and adds it to the existing data. This
            accounts for gaps or overlaps in the data.

            Parameters
            ----------
            newdata : np.ndarray or list
                ECG values for the new packet.
            
            attime : float or int
                The time of the first point in the new ECG signal.
                
            newpeaks : np.ndarray or list, optional
                Array of indices of the peaks in the new signal, by default None.
        """

        # -------------------- Set up variables ------------------------------ #

        # Make sure all data is in the right format.
        if type(newdata)==list: newdata = np.array(newdata)
        if type(newpeaks)==list: newpeaks = np.array(newpeaks)
        if type(attime)==int: attime = float(attime)

        # Make sure that attime is on the fs "grid".
        attime = rounddown(attime, self.diff, self.pr)

        # maxnewtime is the time of the last element in newdata.
        # -1 since the first data point is on attime.
        maxnewtime = attime + (len(newdata)-1) * self.diff

        # These are the times of the newdata points.
        newtimes = roundfloat(
            np.linspace(attime, maxnewtime, len(newdata), endpoint=True),
            self.diff,
            self.pr
            )

        # here1

        # -------------------- Make assertions ------------------------------- #
        
        # Check there are no immediate errors in the data.
        assert (np.diff(newtimes)-self.diff).sum() < 2*self.diff, 'New times diff != 1/fs.'
        assert len(newtimes)==len(newdata)
        if newpeaks is not None:
            assert len(newpeaks)<=len(newdata), 'More peaks than data points.'
            assert len(newpeaks)!=len(newdata), 'Equal number of peaks to data points.'
            assert newpeaks.max()<=len(newdata), 'Peak given after end of new data.'
            assert (newpeaks>=0).all(), 'Negative peaks given.'

        # -------------------- Handle new peaks ------------------------------ #

        # newpeaks_binary is an array the same length as newdata and newtimes. 
        # It is 1 in a position if a peak is given there, and 0 otherwise.
        newpeaks_binary = np.zeros(len(newdata))
        if newpeaks is not None:
            newpeaks_binary[newpeaks] = 1

        # This is a dataframe of the same format as self.df_datatimes containing
        # all of the new data. If self.peaks_given=False then newpeaks_binary is 
        # all zeros.
        new_df_datatimes = pd.DataFrame({
            'data':newdata,
            'time':newtimes,
            'ispeak':newpeaks_binary,
            })

        # -------------------- Handle overlaps ------------------------------- #
        
        # The new data is added to the existing ones. If the times of any of the
        # new data correspond to the old ones, they are overwritten (keep='last').
        # This accounts for overlaps in the data.
        self.df_datatimes = pd.concat([self.df_datatimes, new_df_datatimes]).reset_index(drop=True)
        # Some of the times can get floating-point errors. We make sure to 
        # round to the nearest "diff".
        self.df_datatimes['time'] = self.df_datatimes['time'].apply(
            lambda x: roundfloat(x, self.diff))
        self.df_datatimes.drop_duplicates(
            subset=['time'],
            keep='last',
            inplace=True
            )
        self.df_datatimes.sort_values(by='time', ascending=True, inplace=True)
        self.df_datatimes.reset_index(drop=True, inplace=True)

        self.chop_time_data()

        # -------------------- Handle gaps ----------------------------------- #

        # maxtime is the time of the very last element in self.times once
        # the new times have been joined on. Unless data is sent in a weird 
        # order, this should be equal to maxnewtime.
        # Similar for mintime.
        maxtime = max(max(self.times), maxnewtime) if len(self.times)>0 else maxnewtime
        mintime = min(min(self.times), attime) if len(self.times)>0 else attime
        alltimes = np.linspace(mintime, maxtime, int((maxtime-mintime)*self.fs + 1), endpoint=True)

        # Here we create another dataframe of empty data with all possible times
        # in the range. It fills in any times that are missing, due to gaps 
        # between the packets.
        default_df = pd.DataFrame({
            'data': [np.nan]*len(alltimes),
            'time': alltimes,
            'ispeak': alltimes.copy()*0,
            })
        
        self.df_datatimes = pd.concat([self.df_datatimes,default_df]).reset_index(drop=True)
        # Some of the times can get floating-point errors. We make sure to 
        # round to the nearest "diff".
        self.df_datatimes['time'] = self.df_datatimes['time'].apply(
            lambda x: roundfloat(x, self.diff, self.pr))
        self.df_datatimes.drop_duplicates(
            subset=['time'],
            keep='first',
            inplace=True
            )
        self.df_datatimes.sort_values(by='time', ascending=True, inplace=True)
        self.df_datatimes.reset_index(drop=True, inplace=True)

        # -------------------- Chop and re-cast ------------------------------ #

        # Chop the times that are too old.
        # breakpoint()
        # self.chop_time_data()
        # breakpoint()
        
        
        # Extract the np arrays that are expected in other functions.
        self.data = self.df_datatimes['data'].values
        self.times = roundfloat(self.df_datatimes['time'].values, self.diff)
        peaks_bin = self.df_datatimes['ispeak'].values
        self.peaks = np.where(peaks_bin==1)[0]
        self.peak_amps = self.data[self.peaks]

        # -------------------- Get report times ------------------------------ #

        # These are the times at which to report new features.
        self.new_report_times = np.arange(
                    attime,
                    maxnewtime,
                    self.config['step']
                    )
        self.report_times = np.concatenate([
            self.report_times, self.new_report_times
            ])

        self.new_report_times = roundfloat(self.new_report_times, self.diff, self.pr)
        self.report_times = roundfloat(self.report_times, self.diff, self.pr)
        
        if self.config['lockstep']:
            self.report_times = rounddown(self.report_times, self.config['step'])
            self.new_report_times = rounddown(self.new_report_times, self.config['step'])

        # -------------------------------------------------------------------- #

        self.df_datatimes['realtime'] = self.df_datatimes['time'].apply(
            lambda x: datetime.datetime.fromtimestamp(x).strftime(self.timeformat)
        )

        # Filtering
        # TODO - online filtering
        #self.data, _ = filtering(self.data, fs=self.fs)

    def chop_time_data(self):
        memtime = self.config.get('memory_time', None)
        if memtime is not None:
            cutoff_time = roundfloat(
                self.df_datatimes['time'].max() - memtime,
                self.diff,
                self.pr)
            self.df_datatimes =\
                self.df_datatimes[self.df_datatimes['time']>=cutoff_time]
    
    def get_evaluation_times(self, data, fs):
        pass
    
    def calculate_peaks(self, data, fs):
        peaks = gqrs(data, fs)
        peaks, peak_amps = correct_rpeaks(data, peaks, fs)
        return peaks, peak_amps

    def get_features(self):
        
        if not self.peaks_given:
            print('Finding peaks')
            self.peaks, peak_amps = self.calculate_peaks(self.data, self.fs)
            
        evaluation_times = np.arange(int(self.config['step']), int(len(self.data)/self.fs), int(self.config['step'])).astype(int)

        # Call the various algorithms
        df_dict_all_algos = {}
        for alg in self.config['algorithms']:
            #TODO ensure params are in the right order. Specify?
            print(alg)

            if alg=='ecg':
                print('\t ecg', time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
                df = get_ecg.process(self.config, self.data, self.fs)
                ic()
            
            elif 'ecg_lookback' in alg:
                print('\t ecg_lookback', time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
                lb_number = int(alg[13:]) # After the tag above
                df = get_lookback.process(self.config, df_dict_all_algos['ecg'], lb_number)
                ic()
            
            elif 'shapes' in alg:
                print('\t shapes', time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
                df = get_shapes.process(self.config, self.data, self.fs, self.peaks, evaluation_times, model=self.featuremodels['shapes'][self.lead])
                ic()
            
            elif 'cnn' in alg:
                print('\t cnn', time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
                df = get_cnn.process(self.config, self.data, self.fs, self.peaks, evaluation_times, self.featuremodels['cnn'])
                ic()

            elif 'rr' in alg:
                print('\t rr', time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
                df = get_rr.process(self.config, self.data, self.fs, self.peaks, peak_amps, evaluation_times)
                ic()
            
            else:
                breakpoint()
                raise NotImplementedError
            
            if type(df)!=ErrMessage and len(df)==0:
                df = ErrMessage('tooshort')
            if type(df)==ErrMessage:
                breakpoint()
                return df
            
            # Have a think here, ecg reports times at the end of intervals
            # does this match this class?
            if 'lookback' not in alg:
                # The lookback df already inherits the seconds col from ecg.
                newseconds = df['seconds'].values + self.times.min()
                if self.config['lockstep']:
                    newseconds = rounddown(newseconds,self.config['step'])
                else:
                    breakpoint()
                    raise NotImplementedError
                df['seconds'] = newseconds

            # Check this
            df = df[df['seconds'].isin(self.times)]
        
            df_dict_all_algos[alg] = df
    
        df_list_all_algos = [y[1] for y in sorted(
            df_dict_all_algos.items(), key=lambda x: self.config['algorithms'].index(x[0])
            )]            
        
        df_all = df_list_all_algos[0]
        for df_new in df_list_all_algos[1:]:
            df_all = pd.merge(df_all, df_new, how='inner', on='seconds')

        df_all_report = df_all[df_all['seconds'].isin(self.new_report_times)]

        # Here, if self.new_report_times are not correctly calculated, 
        # we find a proper set of times that do work:
        if len(df_all_report)==0:
            pass
        
        #breakpoint()
        return df_all_report
        #return df_all

def get_new_data(time):
    return {
        'patientID':'NEMC_000',
        'fileID':'0',
        'lead':'II',
        'fs':4.,
        'data':np.random.randn(4*10),
        'time':time
        }


if __name__=='__main__':
    risks = np.array([0.26384273, 0.21093565, 0.29316866, 0.22433963, 0.22695279,
       0.22424032, 0.23668236, 0.25506237, 0.2556145 , 0.29759976,
       0.282409  , 0.27872956, 0.3164461 , 0.22413425, 0.28010863,
       0.22636701, 0.28054997, 0.34711742, 0.3184647 , 0.22047968,
       0.240997  , 0.253928  , 0.25564438, 0.19825812, 0.20457847,
       0.17724851, 0.2058304 , 0.18825959, 0.17383794, 0.17287183,
       0.19963942, 0.18225995, 0.16857232, 0.23367171, 0.16585313,
       0.1823865 , 0.21226552, 0.20319736, 0.1882656 , 0.17129546,
       0.21687204, 0.18405524, 0.18845363, 0.30035308, 0.13224472,
       0.21696272, 0.1894492 , 0.20609808, 0.16281447, 0.18831386,
       0.18275477, 0.16012003, 0.18117276, 0.16888162, 0.16842894,
       0.16813812, 0.16699336, 0.1390421 , 0.20168321])
    unixtimes = np.array([1.59559561e+09, 1.59559562e+09, 1.59559563e+09, 1.59559564e+09,
       1.59559565e+09, 1.59559566e+09, 1.59559567e+09, 1.59559568e+09,
       1.59559569e+09, 1.59559570e+09, 1.59559571e+09, 1.59559572e+09,
       1.59559573e+09, 1.59559574e+09, 1.59559575e+09, 1.59559576e+09,
       1.59559577e+09, 1.59559578e+09, 1.59559579e+09, 1.59559580e+09,
       1.59559581e+09, 1.59559582e+09, 1.59559583e+09, 1.59559584e+09,
       1.59559585e+09, 1.59559586e+09, 1.59559587e+09, 1.59559588e+09,
       1.59559589e+09, 1.59559590e+09, 1.59559591e+09, 1.59559592e+09,
       1.59559593e+09, 1.59559594e+09, 1.59559595e+09, 1.59559596e+09,
       1.59559597e+09, 1.59559598e+09, 1.59559599e+09, 1.59559600e+09,
       1.59559601e+09, 1.59559602e+09, 1.59559603e+09, 1.59559604e+09,
       1.59559605e+09, 1.59559606e+09, 1.59559607e+09, 1.59559608e+09,
       1.59559609e+09, 1.59559610e+09, 1.59559611e+09, 1.59559612e+09,
       1.59559613e+09, 1.59559614e+09, 1.59559615e+09, 1.59559616e+09,
       1.59559617e+09, 1.59559618e+09, 1.59559619e+09])

    with open('live_config.json', 'r') as fopen:
        config = json.load(fopen)

    pat = Patient(
        patientID='p01',
        config=config,
        featuremodels=None,
        timeformat=r"%m/%d/%Y %H:%M:%S"
        )

    pat.update_zadrozny(risks, unixtimes)
    newr = pat.modify_zadrozny(risks, unixtimes)

    print('done')
